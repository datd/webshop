import React from 'react';
import { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { BE_URL } from '../../Constants/UrlConstants';
import style from './NavBar.module.css';
const axios = require('axios').default;

function NavBar(props) {

    function total(){
        return props.cart.cart.reduce(function(total){
            return total+=1;
        }, 0);
    }

    useEffect(function(){
        props.setCart();
    }, []);

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container">
                <Link to="/">
                    <img className={style.wrap_logo_img} src="https://logos-download.com/wp-content/uploads/2016/05/Stop__Shop_logo_logotype.png" alt="" />
                </Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon" />
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item active pl-2 pr-2">
                            <div className="nav-link"><Link style={{color: 'black'}} to="/">Trang chủ</Link></div>
                        </li>
                        <li className="nav-item pl-2 pr-2">
                            <div className="nav-link"><Link style={{color: 'black'}} to="/product-category">Sản phẩm</Link></div>
                        </li>
                        <li className="nav-item pl-2 pr-2">
                            <div className="nav-link"><Link style={{color: 'black'}} to="/">Tin tức</Link></div>
                        </li>
                        <li className="nav-item pl-2 pr-2">
                            <div className="nav-link"><Link style={{color: 'black'}} to="/blog/1">Giới thiệu</Link></div>
                        </li>
                        <li className="nav-item pl-2 pr-2">
                            <div className="nav-link"><Link style={{color: 'black'}} to="/blog/1">Liên hệ</Link></div>
                        </li>
                        <li className="nav-item pl-2 pr-2">
                            <div className="nav-link position-relative">
                                <Link style={{color: 'black'}} to="/cart">
                                    <i className="fa-solid fa-cart-shopping"></i>
                                </Link>
                                <p
                                    style={{
                                        width: '15px',
                                        height: '15px',
                                        position:'absolute',
                                        top: '-3px',
                                        right: '-5px',
                                        fontSize: '12px',
                                        color: 'white',
                                        backgroundColor: 'red',
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderRadius: 999
                                    }}
                                >
                                    {total()}
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    )
}

function mapStateToProps(state){
    return {
        cart: state.cartReducer
    }
}

function mapDispatchToProps(dispatch){
    return {
        setCart: function(){
            axios.get(`${BE_URL}/cart/get-items`, {
                withCredentials:true
            })
            .then(function(res){
                let product = res.data.data;
                dispatch({
                    type: 'SET_CART',
                    product
                });
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);