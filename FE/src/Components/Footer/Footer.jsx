import React from 'react';
import style from './Footer.module.css';

function Footer(props) {
    return (
        <div className={style.footer}>
            <div className="container">
                <div className="row">
                    <div className="col-lg-3">
                        <div className="wrap_logo">
                            <img className='w-100' src="https://logos-download.com/wp-content/uploads/2016/05/Stop__Shop_logo_logotype.png" alt="" />
                        </div>
                        <div className={`text-left ${style.text_col}`}>
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <h5>Menu</h5>
                        <ul className={style.footer_ul}>
                            <li>
                                <div>Trang chủ</div>
                            </li>
                            <li>
                                <div>Sản phẩm</div>
                            </li>
                            <li>
                                <div>Danh mục</div>
                            </li>
                            <li>
                                <div>Tin tức</div>
                            </li>
                            <li>
                                <div>Đăng nhập</div>
                            </li>
                        </ul>
                    </div>
                    <div className="col-lg-3">
                        <h5>Danh mục footer</h5>
                        <ul className={style.footer_ul}>
                            <li>
                                <div>Trang chủ</div>
                            </li>
                            <li>
                                <div>Sản phẩm</div>
                            </li>
                            <li>
                                <div>Danh mục</div>
                            </li>
                            <li>
                                <div>Tin tức</div>
                            </li>
                            <li>
                                <div>Đăng nhập</div>
                            </li>
                        </ul>
                    </div>
                    <div className="col-lg-3">
                        <h5>Danh mục footer</h5>
                        <ul className={style.footer_ul}>
                            <li>
                                <div>Trang chủ</div>
                            </li>
                            <li>
                                <div>Sản phẩm</div>
                            </li>
                            <li>
                                <div>Danh mục</div>
                            </li>
                            <li>
                                <div>Tin tức</div>
                            </li>
                            <li>
                                <div>Đăng nhập</div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Footer;