import {React} from 'react';
import style from './BrandCard.module.css';

export default function BrandCard(props){
    return (
        <div className={style.brand}>
            <img src={props.imageUrl} alt="" />
        </div>
    )
}