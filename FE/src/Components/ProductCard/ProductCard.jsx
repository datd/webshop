import React from 'react';
import { BE_IMG_URL, BE_URL } from '../../Constants/UrlConstants';
import style from './ProductCard.module.css';

export default function ProductCard(props) {
    return (
        <div className={style.product_card}>
            <div className={style.wrap_img}>
                <img src={`${BE_IMG_URL}/uploads/${props.img}`} alt="" />
            </div>
            <div className="content">
                <div className={style.name}>{props.name}</div>
                <div className={style.price}>{props.price.toLocaleString()}đ</div>
            </div>
        </div>
    );
}