import { React, useEffect, useState } from 'react';
import { BE_IMG_URL, BE_URL } from '../../Constants/UrlConstants';
import axios from 'axios';

export default function Carousel() {
    let [sliders, setSliders] = useState();

    console.log(sliders);

    useEffect(function () {
        axios.get(`${BE_URL}/api/slider`)
            .then(function (res) {
                setSliders(res.data.data);
            });
    }, []);

    function renderSlider() {
        return sliders.map(function (value, index) {
            return (
                <div key={index} className={`carousel-item ${index === 1 ? "active" : ""}`}>
                    <img className="d-block w-100" src={`${BE_IMG_URL}/uploads/${value.img}`} alt="First slide" />
                </div>
            );
        });
    }

    return (
        <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
            <div className="carousel-inner" style={{height: '500px'}}>
                {sliders ? renderSlider() : ""}
            </div>
            <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true" />
                <span className="sr-only">Previous</span>
            </a>
            <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true" />
                <span className="sr-only">Next</span>
            </a>
        </div>

    )
}