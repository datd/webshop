import React, { useState } from 'react'
import { Link, NavLink, useNavigate } from 'react-router-dom'
import axios from 'axios'
import { BE_URL } from '../../Constants/UrlConstants'

export default function SideBar() {

    let navigate = useNavigate();

    function logout() {
        axios.get(`${BE_URL}/api/logout`, {
            headers:{
                Authorization: window.localStorage.getItem('access_token')
            }
        }).then(function (res) {
            if (res.data.code === 200) {
                window.localStorage.removeItem('access_token');
                navigate('/admin/login');
            }
        });
    }

    return (
        <div className='card text-left mt-2 bg-dark' style={{ height: '100vh' }}>
            <div className="card-header bg-dark space-between align-items-center">
                <h5 className='m-0 p-0 text-light'>Menu</h5>
                <span className='text-light'><i className="fa-solid fa-bars"></i></span>
            </div>
            <div className="card-body">
                <Link to="/admin/slider">
                    <button
                        className={`btn w-100 mb-2 text-dark space-between`}
                    >
                        <span>Slider</span>
                        <span><i className="fa-brands fa-slideshare"></i></span>
                    </button>
                </Link>
                <Link to="/admin/branch">
                    <button
                        className={`btn w-100 mb-2 text-dark space-between`}
                    >
                        <span>Thương hiệu</span>
                        <span><i className="fa-solid fa-boxes-stacked"></i></span>
                    </button>
                </Link>
                <Link to="/admin/product">
                    <button
                        className={`btn w-100 mb-2 text-dark space-between`}
                    >
                        <span>Sản phẩm</span>
                        <span><i className="fa-brands fa-product-hunt"></i></span>
                    </button>
                </Link>
                <Link to="/admin/order">
                    <button className='btn w-100 mb-2 text-dark text-dark space-between'>
                        <span>Đơn đặt</span>
                        <span><i className="fa-solid fa-file-invoice-dollar"></i></span>
                    </button>
                </Link>
                <Link to="/admin/blog">
                    <button className='btn w-100 mb-2 text-dark text-dark space-between'>
                        <span>Tin tức</span>
                        <span><i className="fa-brands fa-blogger"></i></span>
                    </button>
                </Link>
                <Link target="_blank" to="/">
                    <button className='btn w-100 mb-2 text-dark space-between'>
                        <span>Xem trang ngoài</span>
                        <span><i className="fa-solid fa-users-viewfinder"></i></span>
                    </button>
                </Link>
                <button
                    onClick={() => {
                        logout();
                    }}
                    className='btn w-100 mb-2 btn-danger text-light space-between'
                >
                    <span>Đăng xuất</span>
                    <span><i className="fa-solid fa-arrow-right-from-bracket"></i></span>
                </button>
            </div>
        </div>
    )
}
