import React from 'react'
import { useState } from 'react';
import { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { BE_IMG_URL, BE_URL } from '../../Constants/UrlConstants';
import axios from 'axios';

function CartModal(props) {
    function renderCartList() {
        return props.cart?.cart.map((value, index) => {
            return (
                <tr key={index}>
                    <td>{value.name}</td>
                    <td>
                        <img
                            src={`${BE_IMG_URL}/uploads/${value.options.img}`}
                            alt=""
                            style={{ width: '100px' }}
                        />
                    </td>
                    <td>
                        {(value.price).toLocaleString()}đ
                    </td>
                    <td>
                        <div className="d-flex justify-content-center">
                            <button
                                className="btn btn-sm"
                                onClick={() => { props.updateQty(value.rowId, parseInt(value.qty) - 1) }}
                            >-</button>
                            <input
                                value={value.qty}
                                type="text"
                                style={{ width: '50px' }}
                                className="form-control text-center"
                            />
                            <button
                                className="btn btn-sm"
                                onClick={() => { props.updateQty(value.rowId, parseInt(value.qty) + 1) }}
                            >+</button>
                        </div>
                    </td>
                    <td>
                        { ((value.id).split("_"))[1] }
                    </td>
                    <td>
                        {(value.price * value.qty).toLocaleString()}đ
                    </td>
                    <td>
                        <button
                            className='btn btn-sm'
                            onClick={() => { props.updateQty(value.rowId, 0) }}
                        ><div className="text-danger">Xóa</div></button>
                    </td>
                </tr>
            )
        });
    }

    function total() {
        return props.cart?.cart.reduce(function (total, item) {
            return total + (item.price * item.qty);
        }, 0);
    }

    return (
        <div>
            <div className="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document" style={{ minWidth: '1000px' }}>
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Giỏ hàng</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <table className='table table_vertical'>
                                <thead className='bg-info text-light'>
                                    <tr>
                                        <th>Sản phẩm</th>
                                        <th>Ảnh</th>
                                        <th>Giá</th>
                                        <th>Số lượng</th>
                                        <th>Size</th>
                                        <th>Thành tiền</th>
                                        <th>Xóa</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {props.cart.cart.length > 0 ? renderCartList() : <div className='pt-2'>No data</div>}
                                </tbody>
                            </table>
                        </div>
                        <div className="modal-footer">
                            <div
                                style={{
                                    width: '100%',
                                    display: 'flex',
                                    justifyContent: 'space-between'
                                }}
                            >
                                <div>
                                    <span style={{ fontSize: '25px' }} className='pr-2'>Tổng thanh toán:</span>
                                    <span style={{ fontSize: '25px' }} className=''>{total().toLocaleString()}đ</span>
                                </div>
                                <div>
                                    <button
                                        className={`btn btn-danger mr-2 ${props.cart.cart.length > 0 ? '' : 'disabled'}`}
                                        onClick={() => props.destroyCart()}
                                    >Hủy giỏ hàng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

//Lấy data về bằng hàm này
function mapStateToProps(state) {
    return {
        cart: state.cartReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateQty: function (rowId, qty) {
            let formData = new FormData();
            formData.append('total', qty);
            formData.append('row_id', rowId);
            axios.post(`${BE_URL}/cart/update`, formData, {
                withCredentials: true
            })
                .then(function (res) {
                    //setCart(res.data.data);
                    let product = res.data.data;
                    dispatch({
                        type: 'SET_CART',
                        product
                    });
                });
        },
        destroyCart: function () {
            axios.get(`${BE_URL}/cart/destroy`, {
                withCredentials: true
            })
                .then(function (res) {
                    //setCart(res.data.data);
                    let product = res.data.data;
                    dispatch({
                        type: 'SET_CART',
                        product
                    });
                });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartModal)