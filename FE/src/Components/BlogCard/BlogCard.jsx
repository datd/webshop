import { React } from 'react';
import { BE_IMG_URL, BE_URL } from '../../Constants/UrlConstants';
import style from './BlogCard.module.css';

export default function BlogCard(props) {
    let { img, name, desc } = props.data;
    return (
        <div className={style.blog_card}>
            <div className={style.wrap_img}>
                <img src={`${BE_IMG_URL}/uploads/${img}`} alt="" />
            </div>
            <div className={style.content}>
                <div className={style.title}>{name}</div>
                <div className={style.subtitle}>{desc}</div>
            </div>
        </div>
    )
}