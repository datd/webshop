import {React} from 'react';
import style from './Title.module.css';

export default function Title(props){
    return (
        <div className={style.wrap_title}>
            <div className={style.title}>{props.title}</div>
            <div className={style.desc}>{props.desc}</div>
        </div>
    )
}