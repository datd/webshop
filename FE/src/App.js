import logo from './logo.svg';
import './App.css';
import LayOut from './Pages/OutSide/LayOut/LayOut';

function App() {
  return (
    <div className="App">
      <LayOut></LayOut>
    </div>
  );
}

export default App;
