import axios from 'axios';
import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import Footer from '../../../Components/Footer/Footer';
import NavBar from '../../../Components/NavBar/NavBar';
import { BE_IMG_URL, BE_URL } from '../../../Constants/UrlConstants';
import style from './BlogDetail.module.css';

function BlogDetail(props) {

    let params = useParams();
    let [blogDetail, setBlogDetail] = useState();
    let [otherBlogs, setOtherBlogs] = useState();

    useEffect(function () {
        axios.get(`${BE_URL}/api/blog/${params.id}`)
            .then(function (res) {
                if (res.data.code === 200) {
                    setBlogDetail(res.data.data);
                }
            });
    }, [params]);

    useEffect(function(){
        axios.get(`${BE_URL}/api/blogs`)
            .then(function (res) {
                if (res.data.code === 200) {
                    setOtherBlogs(res.data.data);
                }
            });
    }, []);

    function renderOtherBlogs() {
        return otherBlogs?.data.map(function (value, index) {
            return (
                <Link key={index} style={{ color: 'black' }} to={`/blog/${value.id}`} className='row pb-1'>
                    <div className="col-5">
                        <div className={style.wrap_img}>
                            <img
                                className='w-100'
                                src={`${BE_IMG_URL}/uploads/${value.img}`}
                                alt=""
                            />
                        </div>
                    </div>
                    <div className="col-7">
                        <div><b className={style.name}>{value.name}</b></div>
                        <div className={style.desc}>{value.desc}</div>
                    </div>
                </Link>
            );
        })
    }

    return (
        <div>
            <NavBar></NavBar>
            <div className="container pt-4 pb-4">
                <div className="row">
                    <div className="col-9 text-left">
                        <div><h4>{blogDetail?.name}</h4></div>
                        <div>Tác giả: {blogDetail?.user.name}</div>
                        <div>Đăng lúc: {blogDetail?.creat_at}</div>
                        <hr />
                        <div
                            className="content-root"
                            dangerouslySetInnerHTML={{ __html: blogDetail?.content }}
                        >
                        </div>
                    </div>
                    <div className="col-3 text-left">
                        <h5>Blog khác</h5>
                        <div className='text-left'>
                            {renderOtherBlogs()}
                        </div>
                    </div>
                </div>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default BlogDetail;