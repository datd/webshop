import Footer from '../../../Components/Footer/Footer';
import React, { useEffect } from 'react';
import NavBar from '../../../Components/NavBar/NavBar';
import ProductCard from '../../../Components/ProductCard/ProductCard';
import Title from '../../../Components/Title/Title';
import style from './ProductDetail.module.css';
import { Link, useParams } from 'react-router-dom';
import { useState } from 'react';
import CartModal from '../../../Components/CartModal/CartModal';
import { connect } from 'react-redux';
import { BE_IMG_URL, BE_URL } from '../../../Constants/UrlConstants';
const axios = require('axios').default;

function ProductDetail(props) {
    let params = useParams();
    let [productData, setProductData] = useState();
    let [productList, setProductList] = useState();
    let [preview, setPreview] = useState();
    let [sizeId, setSizeId] = useState("S");
    //let [cart, setCart] = useState();

    useEffect(function () {
        axios.get(`${BE_URL}/api/product/${params.id}`)
            .then(function (res) {
                if (res.data.code === 200) {
                    setProductData(res.data.data);
                    setPreview(res.data.data.img);
                    window.scrollTo(0,0);
                } else if (res.data.code === 404) {
                    alert(404);
                }
            });
    }, [params]);

    useEffect(function(){
        axios.get(`${BE_URL}/api/product`)
            .then(function (res) {
                if (res.data.code === 200) {
                    setProductList(res.data.data);
                }
            });
    }, [])

    function renderProductImages() {
        return productData.product_images.map(function (value, index) {
            return (
                <div key={index} className={`col-lg-3`}>
                    <div
                        style={{ transform: value.img === preview ? 'translateY(-4px)' : '' }}
                        className={`${style.item}`}
                        onClick={() => {
                            setPreview(value.img);
                        }}
                    >
                        <img
                            className='w-100'
                            src={`${BE_IMG_URL}/uploads/${value.img}`}
                            alt=""
                        />
                    </div>
                </div>
            );
        });
    }

    function renderProductList() {
        return productList.data.map(function (value, index) {
            return (
                <div key={index} className="col-lg-3">
                    <Link style={{color:'black'}} to={`/${value.id}/product-detail`}>
                        <ProductCard
                            name={value.name}
                            price={value.price}
                            img={value.img}
                        />
                    </Link>
                </div>
            );
        })
    }

    return (
        <div>
            <NavBar></NavBar>
            <div className='container pt-3 pb-3'>
                <div className="row">
                    <div className="col-lg-5">
                        <div className="wrap_img">
                            <div className={style.big_img}>
                                <img
                                    className='w-100'
                                    src={`${BE_IMG_URL}/uploads/${preview}`}
                                    alt=""
                                />
                            </div>
                            <div className="row">
                                {productData ? renderProductImages() : "No data"}
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-7 text-left">
                        <h3>{productData ? productData.name : "Loading..."}</h3>
                        <h1 className={style.price}>{productData ? productData.price.toLocaleString() : "Loading..."} đ</h1>
                        <p>{productData ? productData.desc : "Loading..."}</p>
                        <div className="d-flex align-items-center pb-3">
                            <span className="pr-2">Size: </span>
                            <select
                                style={{width: '100px'}}
                                className='form-control'
                                onChange={(e)=>{
                                    setSizeId(e.target.value);
                                }}
                            >
                                <option value="S">S</option>
                                <option value="M">M</option>
                                <option value="L">L</option>
                                <option value="XL">XL</option>
                            </select>
                        </div>
                        <div className='d-flex'>
                            <button
                                className='btn btn-info pl-4 pr-4 mr-2'
                                data-toggle="modal"
                                data-target="#exampleModal"
                                onClick={()=>{
                                    let id = productData.id + "_" + sizeId;
                                    props.addToCart(id);
                                }}
                            >Thêm vô giỏ</button>
                            <Link to="/cart">
                                <button className='btn btn-danger pl-4 pr-4'>Giỏ hàng</button>
                            </Link>
                        </div>
                    </div>
                </div>
                <div className="row pt-5 pb-5">
                    <div className="col-12 text-center">
                        <Title
                            title="Sản phẩm khác"
                            desc="Danh sách các sản phẩm khác"
                        />
                    </div>
                    {productList ? renderProductList() : "No data"}
                </div>
            </div>
            <Footer></Footer>
            <CartModal></CartModal>
        </div>
    )
}

function mapStateToProps(){

}

function mapDispatchToProps(dispatch)
{
    return {
        addToCart : function(productId){
            axios.get(`${BE_URL}/cart/${productId}/add-item`, {
                withCredentials: true
            })
            .then(function(res){
                //setCart(res.data.data);
                let product = res.data.data;
                dispatch({
                    type: 'SET_CART',
                    product
                });
            });
        }
    }
}

export default connect(null, mapDispatchToProps)(ProductDetail);