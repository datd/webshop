import { React } from 'react';
import NavBar from '../../../Components/NavBar/NavBar';
import Footer from '../../../Components/Footer/Footer';
import Home from '../Home/Home';
import ProductCategory from '../ProductCategory/ProductCategory';
import ProductDetail from '../ProductDetail/ProductDetail';
import DetailProduct from '../../Admin/DetailProduct/DetailProduct';
import Cart from '../Cart/Cart';
import ToOrder from '../ToOrder/ToOrder';
import {
    BrowserRouter,
    Routes,
    Route,
} from "react-router-dom";
import Login from '../../Admin/Login/Login';
import Slider from '../../Admin/Slider/Slider';
import Product from '../../Admin/Product/Product';
import ProductImage from '../../Admin/ProductImage/ProductImage';
import Order from '../../Admin/Order/Order';
import Branch from '../../Admin/Branch/Branch';
import Blog from '../../Admin/Blog/Blog';
import BlogList from '../../Admin/BlogList/BlogList';
import BlogDetail from '../BlogDetail/BlogDetail';
import BlogEdit from '../../Admin/BlogEdit/BlogEdit';

export default function LayOut() {
    return (
        <div>
            <BrowserRouter>
                <Routes>
                    <Route exact path="/" element={<Home />} />
                    <Route exact path="/product-category" element={<ProductCategory />} />
                    <Route exact path="/:id/product-detail" element={<ProductDetail />} />
                    <Route exact path="/cart" element={<Cart />} />
                    <Route exact path="/to-order" element={<ToOrder />} />
                    <Route exact path="/blog/:id" element={<BlogDetail />} />
                </Routes>
                <Routes>
                    <Route exact path="/admin/login" element={<Login />} />
                    <Route exact path="/admin/slider" element={<Slider />} />
                    <Route exact path="/admin/branch" element={<Branch />} />
                    <Route exact path="/admin/product" element={<Product />} />
                    <Route exact path="/admin/product/:id" element={<DetailProduct />} />
                    <Route exact path="/admin/product/:id/images" element={<ProductImage />} />
                    <Route exact path="/admin/order" element={<Order />} />
                    <Route exact path="/admin/blog" element={<BlogList />} />
                    <Route exact path="/admin/blog/create" element={<Blog />} />
                    <Route exact path="/admin/blog/:id" element={<BlogEdit />} />
                    <Route exact path="/admin" element={<Slider />} />
                </Routes>
            </BrowserRouter>
        </div>
    )
}
