import React from 'react'
import { useEffect } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Footer from '../../../Components/Footer/Footer'
import NavBar from '../../../Components/NavBar/NavBar'
import Title from '../../../Components/Title/Title'
import { BE_IMG_URL, BE_URL } from '../../../Constants/UrlConstants'
import style from './Cart.module.css';
const axios = require('axios').default;

function Cart(props) {

    useEffect(function(){
        props.setCart();
    }, []);

    function total(){
        return props.cart?.cart.reduce(function(total, item){
            return total + (item.price * item.qty);
        }, 0);
    }

    function renderCartItems() {
        return props?.cart.cart.map(function (value, index) {
            return (
                <tr key={index}>
                    <td><b>{value.name}</b></td>
                    <td>
                        <img style={{ width: '100px' }} src={`${BE_IMG_URL}/uploads/${value.options.img}`} alt="" />
                    </td>
                    <td>
                        { ((value.id).split("_"))[1] }
                    </td>
                    <td>
                        <div className="d-flex justify-content-center">
                            <button
                                className="btn btn-sm"
                                onClick={()=>{props.updateQty(value.rowId, parseInt(value.qty) - 1)}}
                            >-</button>
                            <input
                                value={value.qty}
                                type="text"
                                style={{ width: '50px' }}
                                className="form-control text-center"
                            />
                            <button
                                className="btn btn-sm"
                                onClick={()=>{props.updateQty(value.rowId, parseInt(value.qty) + 1)}}
                            >+</button>
                        </div>
                    </td>
                    <td>
                        {value.price.toLocaleString()}đ
                    </td>
                    <td>{(value.qty * value.price).toLocaleString()}đ</td>
                    <td
                        style={{ cursor: 'pointer' }}
                        className='text-danger'
                        onClick={()=>{props.updateQty(value.rowId, 0)}}
                    >Xóa</td>
                </tr>
            );
        })
    }

    return (
        <div>
            <NavBar></NavBar>
            <div className='container text-left pt-4 pb-4'>
                <Title
                    title="GIỎ HÀNG"
                />
                <table className={`table text-center ${style.table_middle}`}>
                    <thead className='bg-info text-light'>
                        <th>Tên</th>
                        <th>Ảnh</th>
                        <th>Size</th>
                        <th>Số lượng</th>
                        <th>Giá</th>
                        <th>Thành tiền</th>
                        <th>Hành động</th>
                    </thead>
                    <tbody>
                        {props.cart.cart.length >0  ? renderCartItems() : <div className='pt-2'>Giỏ hàng rỗng</div>}
                    </tbody>
                </table>
                <div className="text-right">
                    <h5>Tổng tiền: {total().toLocaleString()} đ</h5>
                    <button
                        className={`btn btn-info ${props.cart.cart.length>0 ? "" : "disabled"}`}
                    ><Link className={`${props.cart.cart.length>0 ? "" : "disabled-link"}`} style={{ color: 'white' }} to="/to-order">Tiến hành đặt hàng</Link></button>
                </div>
            </div>
            <Footer></Footer>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        cart: state.cartReducer
    }
}

function mapDispatchToProps(dispatch){
    return {
        updateQty : function(rowId, qty){
            let formData = new FormData();
            formData.append('total', qty);
            formData.append('row_id', rowId);
            axios.post(`${BE_URL}/cart/update`, formData, {
                withCredentials: true
            })
            .then(function(res){
                let product = res.data.data;
                dispatch({
                    type: 'SET_CART',
                    product
                });
            });
        },
        destroyCart: function(){
            axios.get(`${BE_URL}/api/cart/destroy`, {
                withCredentials: true
            })
            .then(function(res){
                let product = res.data.data;
                dispatch({
                    type: 'SET_CART',
                    product
                });
            });
        },
        setCart: function(){
            axios.get(`${BE_URL}/cart/get-items`, {
                withCredentials: true
            })
            .then(function(res){
                let product = res.data.data;
                dispatch({
                    type: 'SET_CART',
                    product
                });
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart)