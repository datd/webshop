import React from 'react'
import { useEffect } from 'react'
import CartModal from '../../../Components/CartModal/CartModal';
import Footer from '../../../Components/Footer/Footer'
import NavBar from '../../../Components/NavBar/NavBar'
import axios from 'axios';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { connect } from 'react-redux';
import { BE_URL } from '../../../Constants/UrlConstants';

function ToOrder(props) {

    let [orderData, setOrderData] = useState();
    let [errors, setErrors] = useState();
    let navigate = useNavigate();

    function handleSetOrderData(name, value){
        let newOrderData = {...orderData};
        newOrderData[name] = value;
        setOrderData(newOrderData);
    }
    
    function createOrder(){
        let formData = new FormData();
        formData.append('name', orderData.name);
        formData.append('email', orderData.email);
        formData.append('address', orderData.address);
        formData.append('phone', orderData.phone);
        axios.post(`${BE_URL}/create-order`, formData, {
            withCredentials : true
        })
        .then(function(res){
            console.log(res.data);
            if(res.data.code === 422){
                setErrors(res.data.message);
            }else if(res.data.code === 200){
                alert('Đặt hàng thành công !');
                navigate('/');
            }
        });
    }

    return (
        <div>
            <NavBar></NavBar>
            <div className='container text-left pt-5 pb-5'>
                <div className="card">
                    <div className="card-header">
                        <h3>Đặt hàng</h3>
                    </div>
                    <div className="card-body">
                        <form action="">
                            <div className="row">
                                <div className="col-6">
                                    <div className="form-group">
                                        <label htmlFor="name">Họ tên</label>
                                        <input
                                            id='name'
                                            type="text"
                                            name='name'
                                            className='form-control'
                                            onChange={(e)=>{
                                                handleSetOrderData(e.target.name, e.target.value);
                                            }}
                                        />
                                        <p className='text-danger font-italic pt-1'>{errors?.name}</p>
                                    </div>
                                </div>
                                <div className="col-6">
                                    <div className="form-group">
                                        <label htmlFor="email">Email</label>
                                        <input
                                            id='email'
                                            type="text"
                                            name='email'
                                            className='form-control'
                                            onChange={(e)=>{
                                                handleSetOrderData(e.target.name, e.target.value);
                                            }}
                                        />
                                        <p className='text-danger font-italic pt-1'>{errors?.email}</p>
                                    </div>
                                </div>
                                <div className="col-6">
                                    <div className="form-group">
                                        <label htmlFor="phone">Số điện thoại</label>
                                        <input
                                            id='phone'
                                            type="text"
                                            name='phone'
                                            className='form-control'
                                            onChange={(e)=>{
                                                handleSetOrderData(e.target.name, e.target.value);
                                            }}
                                        />
                                        <p className='text-danger font-italic pt-1'>{errors?.phone}</p>
                                    </div>
                                </div>
                                <div className="col-6">
                                    <div className="form-group">
                                        <label htmlFor="address">Địa chỉ</label>
                                        <input
                                            id='address'
                                            type="text"
                                            name='address'
                                            className='form-control'
                                            onChange={(e)=>{
                                                handleSetOrderData(e.target.name, e.target.value);
                                            }}
                                        />
                                        <p className='text-danger font-italic pt-1'>{errors?.address}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 text-right">
                                <button
                                    disabled={`${props.cart.cart.length > 0 ? "" : "disabled"}`}
                                    className='btn btn-info pl-4 pr-4'
                                    onClick={(e)=>{
                                        e.preventDefault();
                                        createOrder();
                                    }}
                                >Đặt ngay</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <Footer></Footer>
        </div>
    )
}

function mapStateToProps(state){
    return {
        cart: state.cartReducer
    }
}


export default connect(mapStateToProps)(ToOrder)