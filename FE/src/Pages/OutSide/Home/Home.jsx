import { useEffect } from 'react';
import { useState } from 'react';
import { React } from 'react';
import { Link } from 'react-router-dom';
import BlogCard from '../../../Components/BlogCard/BlogCard';
import BrandCard from '../../../Components/BrandCard/BrandCard';
import Carousel from '../../../Components/Carousel/Carousel';
import Footer from '../../../Components/Footer/Footer';
import NavBar from '../../../Components/NavBar/NavBar';
import ProductCard from '../../../Components/ProductCard/ProductCard';
import Title from '../../../Components/Title/Title';
import { BE_IMG_URL, BE_URL } from '../../../Constants/UrlConstants';
import style from './Home.module.css';
import axios from 'axios';

export default function Home() {

    let [branchs, setBranch] = useState();
    let [products, setProducts] = useState();
    let [blogs, setBlogs] = useState();

    useEffect(function () {
        axios.get(`${BE_URL}/api/branch?limit=6`)
            .then(function (res) {
                setBranch(res.data.data.data);
            });

        axios.get(`${BE_URL}/api/product?limit=8`)
            .then(function (res) {
                setProducts(res.data.data.data);
            });

        axios.get(`${BE_URL}/api/blogs?limit=4`)
            .then(function (res) {
                setBlogs(res.data.data.data);
            });
    }, []);

    function renderBranch() {
        return branchs.map(function (value, index) {
            return (
                <div key={index} className="col-lg-2">
                    <BrandCard imageUrl={`${BE_IMG_URL}/uploads/${value.img}`}></BrandCard>
                </div>
            );
        })
    }

    function renderProduct() {
        return products.map(function (value, index) {
            return (
                <div key={index} className="col-lg-3 pb-2">
                    <Link style={{ color: 'black' }} to={`/${value.id}/product-detail`}>
                        <ProductCard
                            img={value.img}
                            name={value.name}
                            price={value.price}
                        />
                    </Link>
                </div>
            )
        })
    }

    function renderBlogs() {
        return blogs?.map(function (value, index) {
            return (
                <div key={index} className="col-3">
                    <Link style={{color:'black'}} to={`blog/${value.id}`}><BlogCard data={value}/></Link>
                </div>
            )
        })
    }

    return (
        <div>
            <NavBar></NavBar>
            <Carousel></Carousel>
            <div className="container pb-3 pb-3">
                <Title
                    title="Thương hiệu nổi tiếng"
                    desc="Danh sách các thương hiệu nổi tiếng đang kinh doanh"
                ></Title>
                <div className="row align-items-center pt-3 pb-3">
                    {branchs ? renderBranch() : "No Data"}
                </div>
            </div>
            <div className="container pt-4 pb-4">
                <Title
                    title="Các sản phẩm hot"
                    desc="Danh sách các sản phẩm nổi tiếng đang kinh doanh"
                ></Title>
                <div className="row pt-3">
                    {products ? renderProduct() : "No data"}
                </div>
            </div>
            <div className="container pt-5 pb-5">
                <Title
                    title="Thông tin bài viết"
                    desc="Danh sách các bài viết trên website"
                ></Title>
                <div className="row pt-3 pb-3">
                    {renderBlogs()}
                </div>
            </div>
            <Footer></Footer>
        </div>
    );
}