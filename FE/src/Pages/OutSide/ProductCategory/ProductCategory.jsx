import React, { useEffect, useState } from 'react';
import style from './ProductCategory.module.css';
import Carousel from '../../../Components/Carousel/Carousel';
import ProductCard from '../../../Components/ProductCard/ProductCard';
import { Slider, Pagination } from 'antd';
import 'antd/dist/antd.css';
import Title from '../../../Components/Title/Title';
import NavBar from '../../../Components/NavBar/NavBar';
import Footer from '../../../Components/Footer/Footer';
import { Link } from 'react-router-dom';
import { BE_URL } from '../../../Constants/UrlConstants';
const axios = require('axios').default;

function ProductCategory(props) {

    let [page, setPage] = useState(1);
    let [keyword, setKeyword] = useState("");
    let [price, setPrice] = useState("");
    let [branch, setBranch] = useState([]);

    let [api, setApi] = useState({
        keyword: "",
        price: "",
        branch: []
    });
    let [products, setProducts] = useState();
    let [branchs, setBranchs] = useState();

    useEffect(function () {
        axios.get(`${BE_URL}/api/product?limit=12&page=${page}&keyword=${api.keyword}&price=${api.price}&branch=${api.branch}`)
            .then(function (res) {
                setProducts(res.data);
            });
    }, [api, page]);

    useEffect(function () {
        axios.get(`${BE_URL}/api/branch`)
            .then(function (res) {
                setBranchs(res.data.data.data);
            });
    }, []);

    function renderProduct() {
        return products.data.data.map(function (value, index) {
            return (
                <div key={index} className="col-lg-3 pb-2">
                    <Link style={{ color: 'black' }} to={`/${value.id}/product-detail`}>
                        <ProductCard
                            name={value.name}
                            price={value.price}
                            img={value.img}
                        />
                    </Link>
                </div>
            )
        });
    }

    function renderBranchs() {
        return branchs.map(function (value, index) {
            return (
                <div className='d-flex' key={index}>
                    <input
                        id={`input-branch-${index}`}
                        value={value.id}
                        type="checkbox"
                        onChange={(e)=>{
                            let target = e.target;
                            if(target.checked){
                                let index = branch.findIndex(function(id){
                                    return id === target.value;
                                });
                                let newBranch = [...branch];
                                newBranch.push(target.value);
                                if(index === -1){
                                    setBranch(newBranch);
                                }
                            }else{
                                let newBranch = [...branch];
                                let index = newBranch.findIndex(function(id){
                                    return id === target.value;
                                });
                                if(index !== -1){
                                    newBranch.splice(index, 1);
                                    setBranch(newBranch);
                                }
                            }
                        }}
                    />
                    <label className='pl-2' htmlFor={`input-branch-${index}`}>{value.name}</label>
                </div>
            )
        })
    }

    console.log(branch);

    return (
        <div>
            <NavBar></NavBar>
            <div className={style.banner}>
                <img src="https://img.freepik.com/free-photo/banner-web-page-cover-template-clothes-rack-glasses-fashion-shop_41418-4112.jpg?w=2000" alt="" className="w-100" />
            </div>
            <div className="container">
                <div className="row pt-3 pb-3">
                    <div className="col-lg-3">
                        <form 
                            className="card text-left"
                            onSubmit={(e)=>{
                                e.preventDefault();
                                let newApi = { ...api };
                                newApi.keyword = keyword;
                                newApi.price = price;
                                newApi.branch = branch;
                                setApi(newApi);
                            }}
                        >
                            <div className="card-header">Tìm kiếm</div>
                            <div className="card-body">
                                <div className="form-group">
                                    <label htmlFor="">Tên sản phẩm</label>
                                    <input
                                        type="text"
                                        className='form-control'
                                        onChange={(e) => setKeyword(e.target.value)}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="">Khoảng giá: </label>
                                    <Slider
                                        defaultValue={[100000, 1000000]}
                                        range
                                        step={50000}
                                        min={100000}
                                        max={1000000}
                                        onChange={(value) => {
                                            setPrice(`${value[0]}-${value[1]}`);
                                        }}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="">Thương hiệu</label>
                                   
                                        {branchs ? renderBranchs() : "No data"}
                                </div>
                                <button
                                    className='btn btn-sm btn-info'
                                    onClick={(e) => {
                                        e.preventDefault();
                                        let newApi = { ...api };
                                        newApi.keyword = keyword;
                                        newApi.price = price;
                                        newApi.branch = branch;
                                        setApi(newApi);
                                    }}
                                >Tìm kiếm</button>
                            </div>
                        </form>
                    </div>
                    <div className="col-lg-9">
                        <div className="text-left">
                            <Title
                                title="Danh mục sản phẩm"
                            />
                        </div>
                        <div className="row">
                            {products ? renderProduct() : "No data"}
                        </div>
                        <div className="text-right pt-5 pb-3">
                            <Pagination
                                defaultCurrent={1}
                                total={products ? products.data.total : 0}
                                pageSize={products ? products.data.per_page : 12}
                                onChange={(page) => { setPage(page) }}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default ProductCategory;