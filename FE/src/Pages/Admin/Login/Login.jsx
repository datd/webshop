import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { BE_URL } from '../../../Constants/UrlConstants';
const axios = require('axios').default;

export default function Login() {

    let navigate = useNavigate();

    let [loginData, setLoginData] = useState({
        email: "",
        password: ""
    });

    useEffect(function(){
        getMe();
    }, []);

    function login() {
        axios.post(`${BE_URL}/api/admin/login`, loginData)
            .then(function (res) {
                if (res.data.status === false) {
                    alert(res.data.message);
                } else if (res.data.status === true) {
                    window.localStorage.setItem('access_token', "Bearer " + res.data.data);
                    navigate('/admin/slider');
                }
            });
    }

    function getMe(){
        axios.get(`${BE_URL}/api/get-me`, {
            headers: {
                "Authorization": window.localStorage.getItem('access_token')
            }
        })
        .then(function (res) {
            if(res.message !== "Unauthenticated"){
                navigate('/admin/slider');
            }
        });
    }

    function handleSetLoginData(name, value) {
        let newLoginData = { ...loginData };
        newLoginData[name] = value;
        setLoginData(newLoginData);
    }

    return (
        <div className='container text-left'>
            <div className="row pt-5">
                <div className="col-lg-8 m-auto">
                    <div className="card">
                        <div className="card-header"><h5>Đăng nhập</h5></div>
                        <div className="card-body">
                            <form action="">
                                <div className="form-group">
                                    <label htmlFor="">Email</label>
                                    <input
                                        name='email'
                                        type="text"
                                        className='form-control'
                                        onChange={(e) => {
                                            handleSetLoginData(e.target.name, e.target.value)
                                        }}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="">Password</label>
                                    <input
                                        name='password'
                                        type="text"
                                        className='form-control'
                                        onChange={(e) => {
                                            handleSetLoginData(e.target.name, e.target.value)
                                        }}
                                    />
                                </div>
                                <div className="form-group">
                                    <button
                                        className='btn'
                                        onClick={(e) => {
                                            e.preventDefault();
                                            login();
                                        }}
                                    >Đăng nhập</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
