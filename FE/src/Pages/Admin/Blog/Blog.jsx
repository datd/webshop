import React from 'react';
import SideBar from '../../../Components/SideBar/SideBar';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useState } from 'react';
import { useEffect } from 'react';
import {BE_URL} from '../../../Constants/UrlConstants';
import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';
import { getMe } from '../../../Ultils/Ultils';

function Blog(props) {
    let navigate = useNavigate();
    let [blogData, setBlogData] = useState();
    let [previewImg, setPreviewImg] = useState();
    let [errors, setErrors] = useState();

    function handleSetBlogData(name, value){
        let newBlogData = {...blogData};
        newBlogData[name] = value;
        setBlogData(newBlogData);
    }

    function storeBlog(){
        axios.post(`${BE_URL}/api/blog`, blogData, {
            headers:{
                "Authorization": window.localStorage.getItem('access_token'),
                "Content-Type": "multipart/form-data"
            }
        })
        .then(function(res){
            if(res.data.code === 200){
                alert('Tạo thành công !');
                navigate('/admin/blog');
            }else if(res.data.code === 422){
                setErrors(res.data.message);
            }
        });
    }

    useEffect(function(){
        getMe(navigate);
        return function(){
            URL.revokeObjectURL(previewImg);
        }
    }, [previewImg]);

    return (
        <div className='container-fluid pb-4'>
            <div className="row">
                <div className="col-lg-3">
                    <SideBar></SideBar>
                </div>
                <div className="col-lg-9 text-left pt-3">
                    <h5>Quản lý bài viết</h5>
                    <div className="row">
                        <div className="col-7">
                            <div className="form-group">
                                <label htmlFor="">Tiêu đề</label>
                                <input
                                    name='name'
                                    type="text"
                                    className='form-control'
                                    onChange={(e)=>{
                                        handleSetBlogData(e.target.name, e.target.value);
                                    }}
                                />
                                <p className="text-danger font-italic">{errors?.name}</p>
                            </div>
                            <div className="form-group">
                                <label htmlFor="">Mô tả</label>
                                <textarea
                                    name="desc"
                                    className="form-control"
                                    onChange={(e)=>{
                                        handleSetBlogData(e.target.name, e.target.value);
                                    }}
                                ></textarea>
                                <p className="text-danger font-italic">{errors?.desc}</p>
                            </div>
                            <div className="form-group">
                                <label htmlFor="">Trạng thái</label>
                                <select
                                    name="status"
                                    className="form-control"
                                    onChange={(e)=>{
                                        handleSetBlogData(e.target.name, e.target.value);
                                    }}
                                >
                                    <option value="0">Ẩn</option>
                                    <option value="1">Hiện</option>
                                </select>
                                <p className="text-danger font-italic">{errors?.status}</p>
                            </div>
                        </div>
                        <div className="col-5">
                            <img
                                className='w-50'
                                src={previewImg}
                                alt=""
                            />
                            <div className="pt-1">
                                <input
                                    type="file"
                                    name='img'
                                    onChange={(e)=>{
                                        if (e.target.files[0]) {
                                            let blobUrl = URL.createObjectURL(e.target.files[0]);
                                            setPreviewImg(blobUrl);
                                            handleSetBlogData(e.target.name, e.target.files[0]);
                                        } else {
                                            setPreviewImg("");
                                        }
                                    }}
                                />
                                <p className="text-danger font-italic">{errors?.img}</p>
                            </div>
                        </div>
                        <div className="col-12 pt-3">
                            <CKEditor
                                editor={ClassicEditor}
                                onReady={editor => {
                                    // You can store the "editor" and use when it is needed.
                                    console.log('Editor is ready to use!', editor);
                                }}
                                onChange={(event, editor) => {
                                    const data = editor.getData();
                                    //console.log({ event, editor, data });
                                    handleSetBlogData('content', data);
                                }}
                                onBlur={(event, editor) => {
                                    console.log('Blur.', editor);
                                }}
                                onFocus={(event, editor) => {
                                    console.log('Focus.', editor);
                                }}
                            />
                            <p className="text-danger font-italic">{errors?.content}</p>
                            <div className="pt-3 text-right">
                                <Link style={{textDecoration:'none', color: 'black'}} to="/admin/blog">
                                    <button className='btn mr-2'>Quay về</button>
                                </Link>
                                <button
                                    className='btn btn-info'
                                    onClick={()=>{
                                        storeBlog();
                                    }}
                                >Đăng tải</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Blog;