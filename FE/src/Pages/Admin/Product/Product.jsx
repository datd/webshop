import { Pagination } from 'antd'
import React from 'react'
import { useState } from 'react';
import { useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom'
import SideBar from '../../../Components/SideBar/SideBar'
import { BE_IMG_URL, BE_URL } from '../../../Constants/UrlConstants';
import { getMe } from '../../../Ultils/Ultils';
const axios = require('axios').default;

export default function Product() {

    let [errors, setErrors] = useState({
        name: "",
        price: "",
        brand_id: "",
        desc: "",
        image: ""
    });
    let [branchs, setBranch] = useState([]);
    let [createData, setCreateData] = useState({
        name: "",
        price: "",
        branch_id: "",
        desc: "",
        image: null
    });
    let [previewImage, setPreviewImage] = useState();
    let [url, setUrl] = useState({
        keyword: "",
        price: "",
        order_by: "id|desc",
        page: 1,
        branch: ""
    });
    let [listProduct, setListProduct] = useState();
    let navigate = useNavigate();

    useEffect(function () {
        getMe(navigate);
        axios.get(`${BE_URL}/api/branch`, {
            headers: {
                "Authorization": window.localStorage.getItem('access_token')
            }
        })
            .then(function (res) {
                if (res.data.code === 200) {
                    setBranch(res.data.data.data);
                } else {
                    navigate.push('/admin/login');
                }
            });
    }, []);

    useEffect(function () {
        return function () {
            URL.revokeObjectURL(previewImage);
        }
    }, [previewImage]);

    function renderBranchOption() {
        return branchs.map(function (value, index) {
            return (
                <option key={index} value={value.id}>{value.name}</option>
            )
        });
    }

    function handleSetCreateData(name, value) {
        let newData = { ...createData };
        newData[name] = value;
        setCreateData(newData);
    }

    function createProduct() {
        axios.post(`${BE_URL}/api/product`, createData, {
            headers: {
                "Authorization": window.localStorage.getItem('access_token'),
                "Content-Type": "multipart/form-data"
            }
        })
            .then(function (res) {
                console.log(res.data);
                setErrors({
                    name: "",
                    price: "",
                    branch_id: "",
                    desc: "",
                    image: ""
                });
                if (res.data.code === 422) {
                    setErrors(res.data.message);
                } else {
                    setCreateData({
                        name: "",
                        desc: "",
                        price: ""
                    });
                    setPreviewImage(null);
                    alert("Tạo thành công !");
                    getProductList();
                }
            });
    }

    function getProductList() {
        axios.get(`${BE_URL}/api/product?page=${url.page}&keyword=${url.keyword}&price=${url.price}&order_by=${url.order_by}&branch=${url.branch}`, {
            headers: {
                'Authorization': window.localStorage.getItem('access_token')
            }
        })
            .then(function (res) {
                setListProduct(res.data);
                console.log(res.data);
            });
    }

    function deleleProduct(id){
        axios.delete(`${BE_URL}/api/product/${id}`, {
            headers: {
                'Authorization': window.localStorage.getItem('access_token')
            }
        })
        .then(function(res){
            if(res.status === 200){
                getProductList();
            }
        });
    }

    useEffect(function () {
        getProductList();
    }, [url]);

    function renderProductList() {
        return listProduct.data.data.map(function (value, index) {
            return (
                <tr key={index}>
                    <td>{value.id}</td>
                    <td>{value.name}</td>
                    <td>
                        <img
                            style={{ width: '200px' }}
                            src={`${BE_IMG_URL}/uploads/${value.img}`}
                            alt=""
                        />
                    </td>
                    <td>{(value.price).toLocaleString()}đ</td>
                    <td>
                        <div className="d-flex align-items-center justify-content-center">
                            <Link to={`/admin/product/${value.id}`}><button className='btn btn-sm'>Edit</button></Link>
                            <Link to={`/admin/product/${value.id}/images`}>
                                <button className='mr-2 btn btn-sm btn-info'>Ảnh ({value.product_images_count})</button>
                            </Link>
                            {
                                value.product_images_count <= 0
                                ?
                                    <div
                                        style={{cursor:'pointer'}}
                                        className="text-danger btn"
                                        onClick={()=>deleleProduct(value.id)}
                                    >Xóa</div>
                                : ""
                            }
                        </div>
                    </td>
                </tr>
            );
        });
    }

    function handleUpdateUrl(name, value){
        let newUrl = {...url};
        newUrl[name] = value;
        setUrl(newUrl);
    }

    return (
        <div className='container-fluid text-left'>
            <div className="row">
                <div className="col-3">
                    <SideBar></SideBar>
                </div>
                <div className="col-9 pt-3">
                    <div className="row">
                        <div className="col-6">
                            <h4>Tạo sản phẩm</h4>
                            <div>
                                <label htmlFor="">Tên sản phẩm</label>
                                <input
                                    name='name'
                                    value={createData.name}
                                    type="text"
                                    className="form-control"
                                    onChange={(e) => {
                                        handleSetCreateData(e.target.name, e.target.value);
                                    }}
                                />
                                <p className="text-danger font-italic">{errors.name}</p>
                            </div>
                            <div>
                                <label htmlFor="">Giá sản phẩm</label>
                                <input
                                    name='price'
                                    value={createData.price}
                                    type="text"
                                    className="form-control"
                                    onChange={(e) => {
                                        handleSetCreateData(e.target.name, e.target.value);
                                    }}
                                />
                                <p className="text-danger font-italic">{errors.price}</p>
                            </div>
                            <div>
                                <label htmlFor="">Thương hiệu: </label>
                                <select
                                    name="branch_id"
                                    onChange={(e) => {
                                        handleSetCreateData(e.target.name, e.target.value);
                                    }}
                                >
                                    {renderBranchOption()}
                                </select>
                                <p className="text-danger font-italic">{errors.branch_id}</p>
                            </div>
                            <div>
                                <label htmlFor="">Mô tả sản phẩm</label>
                                <textarea
                                    className='form-control'
                                    name='desc'
                                    value={createData.desc}
                                    onChange={(e) => {
                                        handleSetCreateData(e.target.name, e.target.value);
                                    }}
                                ></textarea>
                                <p className="text-danger font-italic">{errors.desc}</p>
                            </div>
                            <button
                                className='btn'
                                onClick={() => {
                                    createProduct();
                                }}
                            >Tạo mới</button>
                        </div>
                        <div className="col-6">
                            <img
                                className='w-50'
                                src={previewImage || "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAMFBMVEXp7vG6vsG3u77s8fTCxsnn7O/f5OfFyczP09bM0dO8wMPk6ezY3eDd4uXR1tnJzdBvAX/cAAACVElEQVR4nO3b23KDIBRA0ShGU0n0//+2KmO94gWZ8Zxmr7fmwWEHJsJUHw8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwO1MHHdn+L3rIoK6eshsNJ8kTaJI07fERPOO1Nc1vgQm2oiBTWJ+d8+CqV1heplLzMRNonED+4mg7L6p591FC+133/xCRNCtd3nL9BlxWP++MOaXFdEXFjZ7r8D9l45C8y6aG0cWtP/SUGhs2d8dA/ZfGgrzYX+TVqcTNRRO9l+fS5eSYzQs85psUcuzk6igcLoHPz2J8gvzWaH/JLS+95RfOD8o1p5CU5R7l5LkfKEp0mQ1UX7hsVXqDpRrifILD/3S9CfmlUQFhQfuFu0STTyJ8gsP3PH7GVxN1FC4t2sbBy4TNRTu7LyHJbqaqKFw+/Q0ncFloo7CjRPwMnCWqKXQZ75El4nKC9dmcJaou9AXOE5UXbi+RGeJygrz8Uf+GewSn9uXuplnWDZJ7d8f24F/s6iq0LYf9olbS3Q8i5oKrRu4S9ybwaQ/aCkqtP3I28QDgeoK7TBya/aXqL5COx67PTCD2grtdOwH+pQV2r0a7YVBgZoKwwIVFQYG6ikMDVRTGByopjD8ATcKb0UhhRTe77sKs2DV7FKSjId18TUEBYVyLhUThWfILHTDqmI85/2RWWjcE/bhP6OD7maT3h20MHsA47JC3PsW0wcwLhv9t0OOPOIkCn21y2bXXwlyylxiYMPk1SuCSmpfK8bNQvIrpAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwNX4BCbAju9/X67UAAAAASUVORK5CYII="}
                                alt=""
                            />
                            <div>
                                <input
                                    type="file"
                                    name='image'
                                    onChange={(e) => {
                                        let blobUrl = URL.createObjectURL(e.target.files[0]);
                                        setPreviewImage(blobUrl);
                                        handleSetCreateData(e.target.name, e.target.files[0]);
                                    }}
                                />
                                <p className="text-danger font-italic">{errors.image}</p>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div className="row pb-4">
                        <div className="filter d-flex pb-2">
                            <input
                                style={{ width: '200px' }}
                                className='form-control mr-2'
                                type="text"
                                placeholder='Tìm kiếm...'
                                name='keyword'
                                onChange={(e)=>{
                                    handleUpdateUrl(e.target.name, e.target.value);
                                }}
                            />
                            <select
                                name='price'
                                style={{ width: '200px' }}
                                className='form-control mr-2'
                                onChange={(e)=>{
                                    handleUpdateUrl(e.target.name, e.target.value);
                                }}
                            >
                                <option value="">Tìm khoảng giá</option>
                                <option value="1000-5000">1000 - 5000</option>
                                <option value="5000-10000">5000 - 10000</option>
                                <option value="10000-999999">Trên 10000</option>
                            </select>
                            <select
                                style={{ width: '200px' }}
                                className='form-control mr-2'
                                name='order_by'
                                onChange={(e)=>{
                                    handleUpdateUrl(e.target.name, e.target.value);
                                }}
                            >
                                <option value="">Sắp xếp</option>
                                <option value="price|desc">Giá giảm dần</option>
                                <option value="price|asc">Giá tăng dần</option>
                            </select>
                            <select
                                style={{ width: '200px' }}
                                className='form-control mr-2'
                                name='branch'
                                onChange={(e)=>{
                                    handleUpdateUrl(e.target.name, e.target.value);
                                }}
                            >
                                <option value="">Thương hiệu</option>
                                {renderBranchOption()}
                            </select>
                        </div>
                        <table className='table table_vertical text-center'>
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên</th>
                                    <th>Ảnh</th>
                                    <th>Giá</th>
                                    <th>Nút bấm</th>
                                </tr>
                            </thead>
                            <tbody>
                                {listProduct ? renderProductList() : "No data"}
                            </tbody>
                        </table>
                        <Pagination
                            total={listProduct ? listProduct.data.total : 1}
                            defaultPageSize={4}
                            onChange={(page)=>{
                                let newUrl = {...url};
                                newUrl.page = page;
                                setUrl(newUrl);
                            }}
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}
