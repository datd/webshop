import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import SideBar from '../../../Components/SideBar/SideBar';
import { BE_IMG_URL, BE_URL } from '../../../Constants/UrlConstants';
import { getMe } from '../../../Ultils/Ultils';
import style from './ProductImage.module.css';
const axios = require('axios').default;

export default function ProductImage() {
    let params = useParams();
    let [productImages, setProductImages] = useState();
    let navigate = useNavigate();

    useEffect(function () {
        getMe(navigate);
        getProductImage();
    }, []);

    function getProductImage(){
        axios.get(`${BE_URL}/api/${params.id}/product-image`, {
            headers: {
                Authorization: window.localStorage.getItem('access_token')
            }
        })
            .then(function (res) {
                setProductImages(res.data);
                console.log(res.data);
            });
    }

    function renderProductImages() {
        return productImages.data.map(function (value, index) {
            return (
                <div key={index} className={style.image_item}>
                    <div className={style.wrap_img}>
                        <img src={`${BE_IMG_URL}/uploads/${value.img}`} alt="" />
                    </div>
                    <button
                        className='w-100 btn btn-sm btn-danger'
                        onClick={()=>{
                            deleteProductImage(value.id);
                        }}
                    >Xóa</button>
                </div>
            );
        });
    }

    function uploadProductImage(files){
        let formData = new FormData();
        for(let i = 0; i< files.length; i++){
            formData.append(`images[${i}]`, files[i]);
        }
        formData.append('product_id', params.id);
        axios.post(`${BE_URL}/api/product-image`, formData,{
            headers: {
                Authorization: window.localStorage.getItem('access_token')
            }
        })
        .then(function(res){
            if(res.data.code === 200){
                getProductImage();
            }
        });
    }

    function deleteProductImage(id){
        axios.delete(`${BE_URL}/api/${id}/product-image`, {
            headers: {
                Authorization: window.localStorage.getItem('access_token')
            }
        })
        .then(function(res){
            if(res.data.code === 200){
                getProductImage();
            }
        });
    }

    return (
        <div>
            <div className='container-fluid text-left'>
                <div className="row">
                    <div className="col-3">
                        <SideBar></SideBar>
                    </div>
                    <div className="col-9 pt-3">
                        <h3>Ảnh sản phẩm</h3>
                        <form action="">
                            <input
                                multiple
                                name='images[]'
                                type="file"
                                onChange={(e)=>{uploadProductImage(e.target.files)}}
                            />
                        </form>
                        <div className={style.wrap_image}>
                            {productImages ? renderProductImages() : "No data"}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
