import { Pagination } from 'antd'
import React from 'react'
import { useState } from 'react';
import { useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom'
import SideBar from '../../../Components/SideBar/SideBar'
import { BE_URL } from '../../../Constants/UrlConstants';
const axios = require('axios').default;

export default function DetailProduct() {
    

    let params = useParams();
    let navigate = useNavigate();
    let [detailProduct, setDetailProduct] = useState();
    let [previewImg, setPreviewImg] = useState();
    let [branchs, setBranchs] = useState();
    let [updateData, setUpdateData] = useState();

    function handleSetUpdateData(name, value){
        let newUpdateData = {...updateData};
        newUpdateData[name] = value;
        setUpdateData(newUpdateData);
    }

    function getMe(){
        axios.get(`${BE_URL}/api/get-me`, {
            headers: {
                "Authorization": window.localStorage.getItem('access_token')
            }
        })
        .then(function (res) {
            if(res.message === "Unauthenticated"){
                navigate('/admin/login');
            }
        })
        .catch(function(res){
            navigate('/admin/login');
        });
    }

    useEffect(function(){
        getMe();
        return function(){
            URL.revokeObjectURL(previewImg);
        }
    }, [previewImg]);

    useEffect(function(){
        axios.get(`${BE_URL}/api/product/${params.id}`, {
            headers: {
                'Authorization': window.localStorage.getItem('access_token')
            }
        })
        .then(function(res){
            if(res.data.code === 200){
                setDetailProduct(res.data.data);
                setUpdateData({
                    name : res.data.data.name,
                    price: res.data.data.price,
                    branch_id: res.data.data.branch_id,
                    desc: res.data.data.desc,
                    img: ""
                });
                setPreviewImg(`${BE_URL}/uploads/`+res.data.data.img);
            }
        });

        axios.get(`${BE_URL}/api/branch`, {
            headers: {
                'Authorization': window.localStorage.getItem('access_token')
            }
        })
        .then(function(res){
            if(res.data.code === 200){
                setBranchs(res.data.data);
            }
        });
    }, []);

    function renderBranchs(){
        return branchs?.data.map(function(value, index){
            return (
                <option
                    key={index}
                    value={value.id}
                    selected={value.id === detailProduct.branch_id ? "selected" : ""}
                >{value.name}</option>
            );
        });
    }

    function updateProduct(){
        axios.post(`${BE_URL}/api/product/${params.id}`, updateData ,{
            headers: {
                'Authorization': window.localStorage.getItem('access_token'),
                "Content-Type": "multipart/form-data"
            }
        })
        .then(function(res){
            if(res.data.code === 200){
                alert("Cập nhật thành công !");
                navigate('/admin/product');
            }
        })
    }

    return (
        <div className='container-fluid text-left'>
            <div className="row">
                <div className="col-3">
                    <SideBar></SideBar>
                </div>
                <div className="col-9 pt-3">
                    <h3>Chi tiết sản phẩm</h3>
                    <div className="row">
                        <div className="col-6">
                            <div className="form-group">
                                <label htmlFor="">Tên sản phẩm</label>
                                <input
                                    type="text"
                                    className='form-control'
                                    value={updateData?.name}
                                    name="name"
                                    onChange={(e)=>{
                                        handleSetUpdateData(e.target.name, e.target.value);
                                    }}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="">Giá</label>
                                <input
                                    type="text"
                                    className='form-control'
                                    value={updateData?.price}
                                    name='price'
                                    onChange={(e)=>{
                                        handleSetUpdateData(e.target.name, e.target.value);
                                    }}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="">Thương hiệu</label>
                                <select
                                    name="branch_id"
                                    className="form-control"
                                    onChange={(e)=>{
                                        handleSetUpdateData(e.target.name, e.target.value);
                                    }}
                                >
                                    {renderBranchs()}
                                </select>
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="wrap-preview-image">
                                <img className='w-50' src={previewImg?previewImg:""} alt="" />
                            </div>
                            <input
                                type="file"
                                name='image'
                                onChange={(e)=>{
                                    let blobUrl = URL.createObjectURL(e.target.files[0]);
                                    setPreviewImg(blobUrl);
                                    handleSetUpdateData(e.target.name, e.target.files[0]);
                                }}
                            />
                        </div>
                        <div className="col-12">
                            <div className="form-group">
                                <label htmlFor="">Mô tả</label>
                                <textarea
                                    name='desc'
                                    className='form-control'
                                    value={updateData?.desc}
                                    onChange={(e)=>{
                                        handleSetUpdateData(e.target.name, e.target.value);
                                    }}
                                ></textarea>
                            </div>
                            <div className="text-right pt-3">
                                <button
                                    className='btn btn-info'
                                    onClick={()=>{updateProduct()}}
                                >Cập nhật</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
