import React from 'react'
import { useState } from 'react';
import { useEffect } from 'react'
import SideBar from '../../../Components/SideBar/SideBar';
import axios from 'axios';
import { Pagination } from 'antd'
import { BE_IMG_URL, BE_URL } from '../../../Constants/UrlConstants';
import { getMe } from '../../../Ultils/Ultils';
import { useNavigate } from 'react-router-dom';

export default function Order() {
    let [orders, setOrders] = useState();
    let [url, setUrl] = useState({
        page: 1,
        keyword: "",
        status: "",
        orderBy: "",
        pageSize: 5
    });
    let [keyword, setKeyword] = useState();
    let [status, setStatus] = useState();
    let [orderBy, setOrderBy] = useState("id|desc");
    let [detailOrder, setDetailOrder] = useState();
    let navigate = useNavigate();

    function handleSetUrl(){
        let newUrl = {...url};
        newUrl.keyword = keyword ? keyword : "";
        newUrl.status = status ? status : "";
        newUrl.orderBy = orderBy ? orderBy : "id|desc";
        newUrl.pageSize = orders.per_page ? orders.per_page : 5;
        setUrl(newUrl);
    }

    useEffect(function () {
        getMe(navigate);
        axios.get(`${BE_URL}/api/orders?limit=${url.pageSize}&page=${url.page}&keyword=${url.keyword}&status=${url.status}&order_by=${url.orderBy}`, {
            headers: {
                Authorization: 'Bearer ' + window.localStorage.getItem('access_token')
            }
        })
        .then(function (res) {
            setOrders(res.data.data);
        });
    }, [url]);

    useEffect(function(){
        window.scrollTo(0,99999);
    }, [detailOrder]);

    function updateOrderStatus(orderId, orderStatus){
        axios.put(`${BE_URL}/api/order/${orderId}/update-status`,{
            status: orderStatus
        },{
            headers: {
                Authorization: 'Bearer ' + window.localStorage.getItem('access_token')
            }
        })
        .then(function (res) {
            console.log(res);
        });
    }

    function getDetailOrder(orderId){
        axios.get(`${BE_URL}/api/order/${orderId}`, {
            headers:{
                Authorization: 'Bearer ' + window.localStorage.getItem('access_token')
            }
        })
        .then(function(res){
            console.log(res.data);
            setDetailOrder(res.data);
        });
    }

    function renderOrders() {
        return orders.data.map(function (value, index) {
            return (
                <tr key={index}>
                    <td>{value.id}</td>
                    <td>{value.name}</td>
                    <td>{value.email}</td>
                    <td>{value.address}</td>
                    <td>{value.phone}</td>
                    <td>
                        <div className="d-flex align-items-center">
                            <select
                                className={`mr-2 ${getBorderColor(value.status)}`}
                                onChange={(e)=>{
                                    updateOrderStatus(value.id, e.target.value);
                                }}
                            >
                                <option
                                    selected={`${value.status === 1 ? "selected" : ""}`}
                                    value="1"
                                    className='text-warning'
                                >Chưa giao</option>
                                <option
                                    selected={`${value.status === 2 ? "selected" : ""}`}
                                    value="2"
                                    className='text-success'
                                >Đã giao</option>
                                <option
                                    selected={`${value.status === 3 ? "selected" : ""}`}
                                    value="3"
                                    className='text-danger'
                                >Đã hủy</option>
                            </select>
                            <button
                                className='btn btn-sm'
                                onClick={()=>{getDetailOrder(value.id)}}
                            >Xem</button>
                        </div>
                    </td>
                </tr>
            )
        })
    }

    function renderDetailOrder(){
        return detailOrder?.products.map(function(value, index){
            return (
                <tr key={index}>
                    <td><b>{value.name}</b></td>
                    <td>
                        <img
                            style={{ width: '100px' }}
                            src={`${BE_IMG_URL}/uploads/${value.img}`}
                            alt=""
                        />
                    </td>
                    <td>{value.pivot.size}</td>
                    <td>{(value.pivot.price).toLocaleString()}đ</td>
                    <td>{value.pivot.qty}</td>
                    <td>{(value.pivot.price * value.pivot.qty).toLocaleString()}đ</td>
                </tr>
            )
        })
    }

    function totalOrder(){
        return detailOrder?.products.reduce(function(total, item){
            return total += item.pivot.price * item.pivot.qty;
        },0);
    }

    function convertStatusToString(status){
        let tranStatus = "";
        if(status === 1){
            tranStatus = <span className='text-warning bg-secondary pl-1 pr-1'>Chưa giao</span>;
        }else if(status === 2){
            tranStatus = <span className='text-light bg-success pl-1 pr-1'>Đã giao</span>;
        }else if(status === 3){
            tranStatus = <span className='text-light bg-danger pl-1 pr-1'>Đã hủy</span>;
        }

        return tranStatus;
    }

    function getBorderColor(status){
        let tranStatus = "";
        if(status === 1){
            tranStatus = "border-warning";
        }else if(status === 2){
            tranStatus = "border-success";
        }else if(status === 3){
            tranStatus = "border-danger";
        }

        return tranStatus;
    }

    return (
        <div>
            <div className='container-fluid text-left'>
                <div className="row">
                    <div className="col-3">
                        <SideBar></SideBar>
                    </div>
                    <div className="col-9 pt-3">
                        <h4>Đơn đặt hàng</h4>
                        <div className="filter pt-2 pb-2">
                            <div className="d-flex">
                                <input
                                    name='keyword'
                                    className='form-control mr-2'
                                    type="text"
                                    placeholder='Tìm kiếm...'
                                    style={{ width: '300px' }}
                                    onChange={(e)=>{
                                        setKeyword(e.target.value);
                                    }}
                                />
                                <select
                                    name='status'
                                    className='form-control mr-2'
                                    style={{ width: '200px' }}
                                    onChange={(e)=>{
                                        setStatus(e.target.value);
                                    }}
                                >
                                    <option value="1">Chưa giao</option>
                                    <option value="2">Đã giao</option>
                                    <option value="3">Hủy</option>
                                    <option value="">Tất cả</option>
                                </select>
                                <select
                                    className='form-control mr-2'
                                    style={{ width: '200px' }}
                                    name="order_by"
                                    onChange={(e)=>{
                                        setOrderBy(e.target.value);
                                    }}
                                >
                                    <option value="id|desc">Mới nhất</option>
                                    <option value="id|asc">Cũ nhất</option>
                                </select>
                                <button
                                    className='btn btn-info'
                                    onClick={()=>{handleSetUrl()}}
                                >Tìm kiếm</button>
                            </div>
                        </div>
                        <table className='table table_vertical'>
                            <thead>
                                <th>ID</th>
                                <th>Người mua</th>
                                <th>Email</th>
                                <th>Địa chỉ</th>
                                <th>Số điện thoại</th>
                                <th>Thao tác</th>
                            </thead>
                            <tbody>
                                {orders ? renderOrders() : <div className='pt-2'>No data</div>}
                            </tbody>
                        </table>
                        <div className="text-right pb-3">
                        <Pagination
                            total={orders ? orders.total : 10}
                            defaultPageSize={url.pageSize}
                            onChange={(page)=>{
                                let newUrl = {...url};
                                newUrl.page = page;
                                setUrl(newUrl);
                            }}
                        />
                        </div>
                        <div className={`${detailOrder ? "" : "d-none"}`}>
                            <h4>Chi tiết</h4>
                            <div className="pb-2">
                                <div>Họ tên: {detailOrder?.name}</div>
                                <div>Địa chỉ: {detailOrder?.address}</div>
                                <div>Số điện thoại: {detailOrder?.phone}</div>
                                <div>Email: {detailOrder?.email}</div>
                                <div>Tình trạng: {convertStatusToString(detailOrder?.status)}</div>
                                <div><b>Tổng thanh toán</b>: {totalOrder()?.toLocaleString()} vnđ</div>
                            </div>
                            <table className='table table_vertical text-center'>
                                <thead>
                                    <th>Tên sản phẩm</th>
                                    <th>Ảnh</th>
                                    <th>Size</th>
                                    <th>Giá</th>
                                    <th>Số lượng</th>
                                    <th>Thành tiền</th>
                                </thead>
                                <tbody>
                                    {renderDetailOrder()}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
