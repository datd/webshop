import React from 'react';
import SideBar from '../../../Components/SideBar/SideBar';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useState } from 'react';
import { useEffect } from 'react';
import {BE_IMG_URL, BE_URL} from '../../../Constants/UrlConstants';
import axios from 'axios';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { getMe } from '../../../Ultils/Ultils';

function BlogEdit(props) {
    let params = useParams();
    let navigate = useNavigate();
    let [blogData, setBlogData] = useState();
    let [previewImg, setPreviewImg] = useState();
    let [errors, setErrors] = useState();

    function handleSetBlogData(name, value){
        let newBlogData = {...blogData};
        newBlogData[name] = value;
        setBlogData(newBlogData);
    }

    function updateBlog(){
        axios.post(`${BE_URL}/api/blog/${params.id}/update`, blogData, {
            headers:{
                "Authorization": window.localStorage.getItem('access_token'),
                "Content-Type": "multipart/form-data"
            }
        })
        .then(function(res){
            if(res.data.code === 200){
                alert('Cập nhật thành công !');
                navigate('/admin/blog');
            }else if(res.data.code === 422){
                setErrors(res.data.message);
            }
        });
    }

    useEffect(function(){
        return function(){
            URL.revokeObjectURL(previewImg);
        }
    }, [previewImg]);

    useEffect(function(){
        getMe(navigate);
        axios.get(`${BE_URL}/api/blog/${params.id}`).then(function(res){
            if(res.data.code === 200){            
                let data = res.data.data;
                setBlogData({
                    name : data.name,
                    status : data.status,
                    desc : data.desc,
                    content: data.content,
                    previewImg: data.img
                });
            }
        });
    }, []);

    console.log(blogData);

    return (
        <div className='container-fluid pb-4'>
            <div className="row">
                <div className="col-lg-3">
                    <SideBar></SideBar>
                </div>
                <div className="col-lg-9 text-left pt-3">
                    <h5>Cập nhật bài viết</h5>
                    <div className="row">
                        <div className="col-7">
                            <div className="form-group">
                                <label htmlFor="">Tiêu đề</label>
                                <input
                                    value={blogData?.name}
                                    name='name'
                                    type="text"
                                    className='form-control'
                                    onChange={(e)=>{
                                        handleSetBlogData(e.target.name, e.target.value);
                                    }}
                                />
                                <p className="text-danger font-italic">{errors?.name}</p>
                            </div>
                            <div className="form-group">
                                <label htmlFor="">Mô tả</label>
                                <textarea
                                    value={blogData?.desc}
                                    name="desc"
                                    className="form-control"
                                    onChange={(e)=>{
                                        handleSetBlogData(e.target.name, e.target.value);
                                    }}
                                ></textarea>
                                <p className="text-danger font-italic">{errors?.desc}</p>
                            </div>
                            <div className="form-group">
                                <label htmlFor="">Trạng thái</label>
                                <select
                                    value={blogData?.status}
                                    name="status"
                                    className="form-control"
                                    onChange={(e)=>{
                                        handleSetBlogData(e.target.name, e.target.value);
                                    }}
                                >
                                    <option value="0">Ẩn</option>
                                    <option value="1">Hiện</option>
                                </select>
                                <p className="text-danger font-italic">{errors?.status}</p>
                            </div>
                        </div>
                        <div className="col-5">
                            <img
                                className='w-50'
                                src={previewImg ? previewImg : `${BE_IMG_URL}/uploads/${blogData?.previewImg}`}
                            />
                            <div className="pt-1">
                                <input
                                    type="file"
                                    name='img'
                                    onChange={(e)=>{
                                        if (e.target.files[0]) {
                                            let blobUrl = URL.createObjectURL(e.target.files[0]);
                                            setPreviewImg(blobUrl);
                                            handleSetBlogData(e.target.name, e.target.files[0]);
                                        } else {
                                            setPreviewImg("");
                                        }
                                    }}
                                />
                                <p className="text-danger font-italic">{errors?.img}</p>
                            </div>
                        </div>
                        <div className="col-12 pt-3">
                            <CKEditor
                                editor={ClassicEditor}
                                data={blogData?.content}
                                onChange={(event, editor) => {
                                    if(blogData){
                                        const data = editor.getData();
                                        handleSetBlogData('content', data);
                                    }
                                }}
                            />
                            <p className="text-danger font-italic">{errors?.content}</p>
                            <div className="pt-3 text-right">
                                <Link style={{textDecoration:'none', color: 'black'}} to="/admin/blog">
                                    <button className='btn mr-2'>Quay về</button>
                                </Link>
                                <button
                                    className='btn btn-info'
                                    onClick={()=>{
                                        updateBlog();
                                    }}
                                >Cập nhật</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default BlogEdit;