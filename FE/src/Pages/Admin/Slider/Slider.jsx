import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import SideBar from '../../../Components/SideBar/SideBar';
import { BE_IMG_URL, BE_URL } from '../../../Constants/UrlConstants';
import { getMe } from '../../../Ultils/Ultils';
import style from './Slider.module.css';
import axios from 'axios';

export default function Product() {
    let navigate = useNavigate();
    let [sliders, setSliders] = useState();
    let [errors, setErrors] = useState();

    useEffect(function () {
        getMe(navigate);
        getSlider();
    }, []);

    function getSlider(){
        axios.get(`${BE_URL}/api/slider`, {
            headers: {
                "Authorization": window.localStorage.getItem('access_token')
            }
        })
        .then(function (res) {
            setSliders(res.data.data);
        });
    }

    function uploadSlider(file){
        axios.post(`${BE_URL}/api/slider`, {
            image: file
        }, {
            headers: {
                "Content-Type": "multipart/form-data",
                "Authorization": window.localStorage.getItem('access_token')
            }
        })
        .then(function(res){
            if(res.data.code === 200){
                getSlider();
                setErrors();
            }else if(res.data.code === 422){
                setErrors(res.data.message);
            }
        });
    }

    function deleteSlider(id){
        let url = `${BE_URL}/api/slider/${id}/delete`;
        axios.delete(url, {
            headers: {
                "Authorization": window.localStorage.getItem('access_token')
            }
          })
        .then(function(){
            alert("Xóa thành công !");
            getSlider();
        });
    }

    function renderSliderItems() {
        return sliders.map(function (value, index) {
            return (
                <tr key={index}>
                    <td>123</td>
                    <td>
                        <img
                            style={{ width: '200px' }}
                            src={`${BE_IMG_URL}/uploads/${value.img}`}
                            alt=""
                        />
                    </td>
                    <td>
                        <div>10:10 2000-20-20</div>
                        <div>10:10 2000-20-20</div>
                    </td>
                    <td>
                        <button className='btn btn-sm btn-danger' onClick={()=>{deleteSlider(value.id)}}>Xóa</button>
                    </td>
                </tr>
            );
        });
    }

    return (
        <div className='container-fluid'>
            <div className="row">
                <div className="col-lg-3">
                    <SideBar></SideBar>
                </div>
                <div className="col-lg-9 text-left pt-3">
                    <h5>Quản lý slider</h5>
                    <div className="row">
                        <div className="col-6">
                            <form action="">
                                <label htmlFor="">Chọn ảnh</label>
                                <input onChange={(e)=>{
                                    uploadSlider(e.target.files[0]);
                                }} type="file" />
                                <p className="text-danger font-italic">{errors ? errors.image : ""}</p>
                            </form>
                        </div>
                    </div>
                    <table className={`table ${style.table_middle}`}>
                        <thead>
                            <th>ID</th>
                            <th>Ảnh</th>
                            <th>Timesteamps</th>
                            <th>Xóa</th>
                        </thead>
                        <tbody>
                            {sliders ? renderSliderItems() : "No data"}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}
