import { Pagination } from 'antd';
import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import SideBar from '../../../Components/SideBar/SideBar';
import axios from 'axios';
import { BE_IMG_URL, BE_URL } from '../../../Constants/UrlConstants';
import { useState } from 'react';
import { useEffect } from 'react';
import { getMe } from '../../../Ultils/Ultils';

function BlogList(props) {

    let [blogs, setBlogs] = useState();
    let [keyword, setKeyword] = useState("");
    let [status, setStatus] = useState("");
    let [orderBy, setOrderBy] = useState("id|desc");
    let [page, setPage] = useState(1);
    let [url, setUrl] = useState({
        keyword: "",
        orderBy: "id|desc",
        status: ""
    })
    let navigate = useNavigate();

    function getBlogList() {
        axios.get(`${BE_URL}/api/blog?page=${page}&keyword=${url.keyword}&status=${url.status}&order_by=${url.orderBy}`, {
            headers: {
                Authorization: window.localStorage.getItem('access_token')
            }
        })
            .then(function (res) {
                setBlogs(res.data.data);
            });
    }

    function handleSetUrl(){
        let newUrl = {};
        newUrl.keyword = keyword;
        newUrl.status = status;
        newUrl.orderBy = orderBy;
        setUrl(newUrl);
    }

    useEffect(function () {
        getMe(navigate);
        getBlogList();
    }, [url, page]);

    function updateBlogStatus(blogId, status){
        axios.put(`${BE_URL}/api/blog/${blogId}/update-status`,{
            status: status
        },{
            headers:{
                Authorization: window.localStorage.getItem('access_token')
            }
        })
        .then(function(res){
            if(res.data.code === 200){
                getBlogList();
            }
        })
    }

    function deleteBlog(blogId){
        axios.delete(`${BE_URL}/api/blog/${blogId}`, {
            headers:{
                Authorization: window.localStorage.getItem('access_token')
            }
        })
        .then(function(res){
            if(res.data.code === 200){
                getBlogList();
            }
        })
    }

    function renderBlog() {
        return blogs?.data.map(function (value, index) {
            return (
                <tr key={index}>
                    <td>{value.id}</td>
                    <td>{value.name}</td>
                    <td>
                        <img
                            style={{ width: '100px' }}
                            src={`${BE_IMG_URL}/uploads/${value.img}`}
                        />
                    </td>
                    <td>
                        <input
                            style={{
                                width: '20px',
                                height: '20px',
                                cursor: 'pointer'
                            }}
                            checked={value.status === 1 ? "checked" : ""}
                            onChange={(e)=>{
                                updateBlogStatus(value.id, e.target.checked ? 1 : 0);
                            }}
                            type="checkbox"
                        />
                    </td>
                    <td>{value.view_count}</td>
                    <td>
                        <Link to={`/admin/blog/${value.id}`}>
                            <button className='btn btn-sm mr-1'>Edit</button>
                        </Link>
                        <button
                            className='btn text-danger btn-sm mr-1'
                            onClick={()=>{
                                deleteBlog(value.id);
                            }}
                        >Gỡ bỏ</button>
                    </td>
                </tr>
            );
        });
    }

    return (
        <div className='container-fluid pb-4'>
            <div className="row">
                <div className="col-lg-3">
                    <SideBar></SideBar>
                </div>
                <div className="col-lg-9 text-left pt-3">
                    <h5>Quản lý bài viết</h5>
                    <div className="row">
                        <div className="col-12 text-right pb-2">
                            <Link to="/admin/blog/create">
                                <button className='btn btn-sm btn-info'>+ Tạo mới bài viết</button>
                            </Link>
                        </div>
                        <div className="col-12">
                            <div className="d-flex pb-2">
                                <input
                                    name='keyword'
                                    type="text"
                                    className='form-control mr-1'
                                    style={{width: '200px'}}
                                    placeholder="Tìm kiếm..."
                                    onChange={(e)=>{
                                        setKeyword(e.target.value);
                                    }}
                                />
                                <select
                                    name="status"
                                    className="form-control mr-1"
                                    style={{width: '200px'}}
                                    onChange={(e)=>{
                                        setStatus(e.target.value);
                                    }}
                                >
                                    <option value="">Tất cả</option>
                                    <option value="0">Ẩn</option>
                                    <option value="1">Hiện</option>
                                </select>
                                <select
                                    name="order_by"
                                    className="form-control mr-1"
                                    style={{width: '200px'}}
                                    onChange={(e)=>{
                                        setOrderBy(e.target.value);
                                    }}
                                >
                                    <option value="id|desc">Mới nhất</option>
                                    <option value="id|asc">Cũ nhất</option>
                                    <option value="view_count|desc">Xem nhiều nhất</option>
                                    <option value="view_count|asc">Xem ít nhất</option>
                                </select>
                                <button
                                    className='btn'
                                    onClick={()=>{
                                        handleSetUrl();
                                    }}
                                >Tìm kiếm</button>
                            </div>
                            <table className="table table_vertical text-center">
                                <thead className='bg-info text-light'>
                                    <tr>
                                        <th>ID</th>
                                        <th>Tiêu đề</th>
                                        <th>Img</th>
                                        <th>Ẩn/hiện</th>
                                        <th>Lượt xem</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {renderBlog()}
                                </tbody>
                            </table>
                            <Pagination
                                current={blogs ? blogs.current_page : 1}
                                total={blogs ? blogs.total : 10}
                                defaultPageSize={blogs ? blogs.per_page : 10}
                                onChange={(page) => {
                                    setPage(page);
                                }}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default BlogList;