import { Pagination } from 'antd';
import React, { useEffect } from 'react'
import { useState } from 'react';
import SideBar from '../../../Components/SideBar/SideBar'
import style from './Branch.module.css';
import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';
import { BE_IMG_URL, BE_URL } from '../../../Constants/UrlConstants';
import { getMe } from '../../../Ultils/Ultils';

export default function Branch() {
    let [previewCreateImage, setPreviewCreateImage] = useState();
    let [previewUpdateImage, setPreviewUpdateImage] = useState();
    let [createBranchData, setCreateBranchData] = useState();
    let [updateBranchData, setUpdateBranchData] = useState();
    let [errors, setErrors] = useState();
    let [branchs, setBranchs] = useState();
    let [url, setUrl] = useState({
        page: 1
    });
    let navigate = useNavigate();

    function updateBranch() {
        axios.post(`${BE_URL}/api/branch/${updateBranchData.id}`, updateBranchData, {
            headers: {
                "Authorization": window.localStorage.getItem('access_token'),
                "Content-Type": "multipart/form-data"
            }
        })
        .then(function (res) {
            if (res.data.code === 200) {
                alert("Cập nhật thành công !");
                handleSetUpdateBranch("name", "");
                URL.revokeObjectURL(previewUpdateImage);
                setPreviewUpdateImage("");
                handleSetUrl('page', 1);
            }
        })
    }

    function handleSetCreateBranch(name, value) {
        let newCreateBranchData = { ...createBranchData };
        newCreateBranchData[name] = value;
        setCreateBranchData(newCreateBranchData);
    }

    function handleSetUpdateBranch(name, value) {
        let newUpdateBranchData = { ...updateBranchData };
        newUpdateBranchData[name] = value;
        setUpdateBranchData(newUpdateBranchData);
    }

    function createBranch() {
        axios.post(`${BE_URL}/api/branch`, createBranchData, {
            headers: {
                "Authorization": window.localStorage.getItem('access_token'),
                "Content-Type": "multipart/form-data"
            }
        })
            .then(function (res) {
                if (res.data.code === 200) {
                    alert('Tạo thành công !');
                    handleSetCreateBranch("name", "");
                    URL.revokeObjectURL(previewCreateImage);
                    setPreviewCreateImage("");
                    setErrors();
                    handleSetUrl('page', 1);
                } else if (res.data.code === 422) {
                    setErrors(res.data.message);
                }
            });
    }

    useEffect(function () {
        return function () {
            URL.revokeObjectURL(previewCreateImage);
        }
    }, [previewCreateImage]);

    useEffect(function () {
        getMe(navigate);
        getBranch();
    }, [url]);

    function handleSetUrl(name, value) {
        let newUrl = { ...url };
        newUrl[name] = value;
        setUrl(newUrl);
    }

    function getBranch() {
        axios.get(`${BE_URL}/api/branch?limit=5&page=${url?.page}`, {
            headers: {
                Authorization: window.localStorage.getItem('access_token')
            }
        })
            .then(function (res) {
                setBranchs(res.data);
            });
    }

    function deleteBranch(branchId) {
        axios.delete(`${BE_URL}/api/branch/${branchId}`, {
            headers: {
                Authorization: window.localStorage.getItem('access_token')
            }
        })
            .then(function (res) {
                if (res.data.code === 200) {
                    alert("Xóa thành công !");
                    handleSetUrl("page", 1);
                }
            })
    }

    function renderBranch() {
        return branchs?.data.data.map(function (value, index) {
            return (
                <tr key={index}>
                    <td><b>{value.id}</b></td>
                    <td>{value.name}</td>
                    <td>
                        <img
                            style={{ width: '200px' }}
                            src={`${BE_IMG_URL}/uploads/${value.img}`}
                        />
                    </td>
                    <td>
                        <div>{value.created_at}</div>
                        <div>{value.updated_at}</div>
                    </td>
                    <td>
                        <div className='d-flex align-items-center justify-content-center'>
                            <button
                                className='btn btn-sm btn-warning mr-2'
                                onClick={() => {
                                    let newUpdateBranchData = { ...updateBranchData };
                                    newUpdateBranchData.name = value.name;
                                    newUpdateBranchData.id = value.id;
                                    setUpdateBranchData(newUpdateBranchData);
                                    setPreviewUpdateImage(`${BE_URL}/uploads/${value.img}`);
                                    window.scrollTo(0, 0);
                                }}
                            >Sửa</button>
                            {
                                value.products_count === 0
                                    ? <div
                                        className='text-danger btn'
                                        onClick={() => {
                                            deleteBranch(value.id);
                                        }}
                                    >Xóa</div>
                                    : <Link to="/admin/product">Sản phẩm</Link>
                            }
                        </div>
                    </td>
                </tr>
            );
        });
    }

    return (
        <div className='container-fluid pb-4'>
            <div className="row">
                <div className="col-lg-3">
                    <SideBar></SideBar>
                </div>
                <div className="col-lg-9 text-left pt-3">
                    <h5>Quản lý Branch</h5>
                    <div className="row">
                        <div className="col-6">
                            <form action="" className='card'>
                                <div className="card-header">Tạo thương hiệu</div>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-6">
                                            <div className="form-group">
                                                <label htmlFor="">Tiêu đề</label>
                                                <input
                                                    value={createBranchData?.name}
                                                    type="text"
                                                    className='form-control'
                                                    name='name'
                                                    onChange={(e) => {
                                                        handleSetCreateBranch(e.target.name, e.target.value);
                                                    }}
                                                />
                                                <p className='text-danger font-italic'>{errors?.name}</p>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="">Ảnh</label>
                                                <input
                                                    type="file"
                                                    name='image'
                                                    onChange={(e) => {
                                                        if (e.target.files[0]) {
                                                            let blobUrl = URL.createObjectURL(e.target.files[0]);
                                                            setPreviewCreateImage(blobUrl);
                                                            handleSetCreateBranch(e.target.name, e.target.files[0]);
                                                        } else {
                                                            setPreviewCreateImage("");
                                                        }
                                                    }}
                                                />
                                                <p className='text-danger font-italic'>{errors?.image}</p>
                                            </div>
                                            <div className="text-right">
                                                <button
                                                    className='btn btn-sm btn-success'
                                                    onClick={(e) => {
                                                        e.preventDefault();
                                                        createBranch();
                                                    }}
                                                >Lưu lại</button>
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className={style.wrap_image}>
                                                <img
                                                    className='w-100'
                                                    src={previewCreateImage}
                                                    alt=""
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="col-6">
                            <form action="" className='card'>
                                <div className="card-header bg-warning">Edit thương hiệu</div>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-6">
                                            <div className="form-group">
                                                <label htmlFor="">Tiêu đề</label>
                                                <input
                                                    value={updateBranchData?.name}
                                                    type="text"
                                                    className='form-control'
                                                    name='name'
                                                    onChange={(e) => {
                                                        handleSetUpdateBranch(e.target.name, e.target.value);
                                                    }}
                                                />
                                                <p className='text-danger font-italic'>{errors?.name}</p>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="">Ảnh</label>
                                                <input
                                                    type="file"
                                                    name='image'
                                                    onChange={(e) => {
                                                        if (e.target.files[0]) {
                                                            let blobUrl = URL.createObjectURL(e.target.files[0]);
                                                            setPreviewUpdateImage(blobUrl);
                                                            handleSetUpdateBranch(e.target.name, e.target.files[0]);
                                                        } else {
                                                            setPreviewUpdateImage("");
                                                        }
                                                    }}
                                                />
                                                <p className='text-danger font-italic'>{errors?.image}</p>
                                            </div>
                                            <div className="text-right">
                                                <button
                                                    className='btn btn-sm btn-warning'
                                                    onClick={(e) => {
                                                        e.preventDefault();
                                                        updateBranch();
                                                    }}
                                                >Cập nhật</button>
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className={style.wrap_image}>
                                                <img
                                                    className='w-100'
                                                    src={previewUpdateImage}
                                                    alt=""
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 pt-3">
                            <table className='table table_vertical text-center'>
                                <thead className='bg-info text-light'>
                                    <th>ID</th>
                                    <th>Tiêu đề</th>
                                    <th>Ảnh</th>
                                    <th>Timestamps</th>
                                    <th>Xóa</th>
                                </thead>
                                <tbody>
                                    {renderBranch()}
                                </tbody>
                            </table>
                            <div className="pt-3">
                                <Pagination
                                    current={url?.page}
                                    total={branchs ? branchs.data.total : 999}
                                    defaultPageSize={branchs ? branchs.data.per_page : 5}
                                    onChange={(page) => {
                                        handleSetUrl('page', page);
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
