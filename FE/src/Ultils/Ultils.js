import axios from "axios";
import { BE_URL } from "../Constants/UrlConstants";
export function getMe(navigate){
    axios.get(`${BE_URL}/api/get-me`, {
        headers: {
            "Authorization": window.localStorage.getItem('access_token')
        }
    })
    .then(function (res) {
        if(res.message === "Unauthenticated"){
            navigate('/admin/login');
        }
    })
    .catch(function(res){
        navigate('/admin/login');
    });
}