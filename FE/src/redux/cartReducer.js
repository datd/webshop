const initState = {
    cart: [
        {
            rowId: "",
            id: "",
            name: "",
            price: 0,
            options: {
                "img" : ""
            },
            qty: 0
        }
    ]
}

const cartReducer = (state = initState, action) => {
    switch (action.type) {
        case 'SET_CART':
            state.cart = action.product;
            console.log(state.cart);
            return {...state};
        default:
            break;
    }
    return {...state};
}

export default cartReducer;