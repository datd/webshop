<?php

namespace App\Traits;

trait ResponseTrait
{
    public function success($data)
    {
        return response()->json([
            'status' => true,
            'code' => 200,
            'data' => $data
        ], 200);
    }

    public function fail($code, $message)
    {
        return response()->json([
            'status' => false,
            'code' => $code,
            'message' => $message
        ], 200);
    }
}