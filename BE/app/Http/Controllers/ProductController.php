<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function store(CreateProductRequest $request)
    {
        $file = $request->file('image');
        $filePath = $file->store('products', ['disk' => 'upload']);
        $product = Product::create([
            'name' => $request['name'],
            'price' => $request['price'],
            'branch_id' => $request['branch_id'],
            'desc' => $request['desc'],
            'img' => $filePath
        ]);

        return $this->success($product);
    }

    public function getList(Request $request)
    {
        $products = Product::when(!empty($request['keyword']), function($q) use($request) {
            $q->where('name', 'like', "%" . $request['keyword'] . "%");
        })
        ->when(!empty($request['price']), function($q) use($request) {
            $price = explode("-", $request['price']);
            $q->whereBetween('price', $price);
        })
        ->when(!empty($request['order_by']), function($q) use($request){
            $orderBy = explode("|", $request['order_by']);
            $q->orderBy($orderBy[0], $orderBy[1]);
        })
        ->when(!empty($request['branch']), function($q) use($request){
            $branch_ids = explode(",", $request['branch']);
            $q->whereIn('branch_id', $branch_ids);
        })
        ->withCount('productImages')
        ->paginate($request['limit'] ?? 4);

        return $this->success($products);
    }

    public function delete($id)
    {
        $product = Product::find($id) ?? null;
        Storage::disk('upload')->delete($product->img);
        return $product->delete();
    }

    public function getDetailProduct($productId)
    {
        $product = Product::where('id', $productId)
            ->with('productImages')
            ->first();

        return $this->success($product);
    }

    public function update(UpdateProductRequest $request, $productId)
    {
        $product = Product::find($productId);
        if($request->hasFile('image')){
            Storage::disk('upload')->delete($product->img);
            $product->img = $request->file('image')->store('products', ['disk' => 'upload']);
        }
        if(isset($request['branch_id'])){
            $product->branch_id = $request['branch_id'];
        }
        $product->name = $request['name'];
        $product->price = $request['price'];
        $product->desc = $request['desc'];
        $result = $product->save();
        return $this->success($result);
    }
}
