<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderRequest;
use App\Jobs\ConfirmOrderJob;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use Cart;

class OrderController extends Controller
{
    protected $order;
    protected $cart;

    public function __construct(Order $order, Cart $cart)
    {
        $this->order = $order;
        $this->cart = $cart;
    }

    public function createOrder(CreateOrderRequest $request)
    {
        $cart = array_values($this->cart::content()->toArray());
        $order = $this->order->create([
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'address' => $request['address'],
            'status' => 1
        ]);
        foreach($cart as $item){
            $productId = explode("_", $item['id'])[0];
            $productSize = explode("_", $item['id'])[1];
            $order->products()->attach($productId, [
                'qty' => $item['qty'],
                'price' => $item['price'],
                'size' => $productSize
            ]);
        }
        $orderDetail = $this->getDetailOrder($order->id);
        ConfirmOrderJob::dispatch($order->email, $orderDetail->toArray());
        $this->cart::destroy();
        return $this->success($this->getDetailOrder($order->id));
    }

    public function getDetailOrder($orderId)
    {
        return $this->order::where('id', $orderId)
            ->with(['products'=>function($q){
                $q->withPivot('qty', 'price', 'size');
            }])
            ->first();
    }

    public function getListOrder(Request $request)
    {
        $orders = $this->order::when(!empty($request['keyword']), function($q) use($request){
            $q->where('name', 'like', "%" . $request['keyword'] . "%");
        })
        ->when(!empty($request['status']), function($q) use($request) {
            $q->where('status', $request['status']);
        })
        ->when(!empty($request['order_by']), function($q) use($request){
            $orderBy = explode("|", $request['order_by']);
            $q->orderBy($orderBy[0], $orderBy[1]);
        })
        ->orderBy('id', 'desc')
        ->paginate($request['limit'] ?? 10);

        return $this->success($orders);
    }

    public function updateOrderStatus(Request $request, $orderId)
    {
        $order = Order::find($orderId)->update([
            'status' => $request['status']
        ]);
        return $this->success($order);
    }
}
