<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    protected $cart = null;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    // public function addItem($productID)
    // {
    //     $product = Product::find($productID);
    //     $this->cart::add($productID, $product->name, 1, $product->price, 1,['img' => $product->img]);
    //     return $this->getItems();
    // }

    public function addItem($productID)
    {
        $product = Product::find($productID);
        $this->cart::add($productID, $product->name, 1, $product->price, 1,[
            'img' => $product->img
        ]);
        return $this->getItems();
    }

    public function update(Request $request)
    {
        $this->cart::update($request['row_id'], $request['total']);
        return $this->getItems();
    }

    public function remove($rowID)
    {
        $this->cart::remove($rowID);
    }

    public function getItems()
    {
        $cart = array_values($this->cart::content()->toArray());
        return $this->success(($cart));
    }

    public function destroy()
    {
        $this->cart::destroy();
        return $this->getItems();
    }
}
