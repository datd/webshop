<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductImageRequest;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductImageController extends Controller
{
    public function getList($productId)
    {
        $productImages = ProductImage::where('product_id', $productId)->get();
        return $this->success($productImages);
    }

    public function store(CreateProductImageRequest $request)
    {
        $files = $request->file('images');
        $productId = $request->product_id;
        foreach($files as $file){
            $filePath = $file->store('product_images', ['disk' => 'upload']);
            ProductImage::create([
                'product_id' => $productId,
                'img' => $filePath
            ]);
        }

        return $this->success("Tạo thành công!");
    }

    public function delete($productImageID)
    {
        $productImage = ProductImage::find($productImageID) ?? null;
        Storage::disk('upload')->delete($productImage->img);
        $res = $productImage->delete();
        return $this->success($res);
    }
}
