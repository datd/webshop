<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSliderRequest;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    public function store(CreateSliderRequest $request)
    {
        $file = $request->file('image');
        $filePath = $file->store('sliders', ['disk' => 'upload']);
        $slider = Slider::create([
            'img' => $filePath
        ]);

        return $this->success($slider);
    }

    public function get(Request $request)
    {
        $slider = Slider::all();
        return $this->success($slider);
    }

    public function delete($id)
    {
        $slider = Slider::find($id) ?? null;
        Storage::disk('upload')->delete($slider->img);
        return $slider->delete();
    }
}
