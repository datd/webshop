<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticationController extends Controller
{
    public function login(Request $request)
    {
        $loginData = $request->only('email', 'password');
        if(Auth::attempt($loginData)){
            $user = User::where('email', $loginData['email'])->first();
            $token = $user->createToken('login-token')->plainTextToken;
            return $this->success($token);
        }else{
            return $this->fail(400, "Sai email hoặc mật khẩu !");
        }
    }

    public function getMe()
    {
        return Auth::user();
    }

    public function logout()
    {
        $user = User::find(auth()->user()->id ?? null);
        if($user) $user->tokens()->delete();
        return $this->success("Đăng xuất thành công !");
    }
}
