<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBlogRequest;
use App\Http\Requests\UpdateBlogRequest;
use Illuminate\Http\Request;
use App\Models\Blog;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    public function getList(Request $request)
    {
        $blogs = Blog::when(!empty($request['keyword']), function($q) use($request){
            $q->where('name', 'like', "%" . $request['keyword'] . "%");
        })
        ->when(isset($request['status']) && $request['status'] != "", function($q) use($request){
            $q->where('status', $request['status']);
        })
        ->when(!empty($request['order_by']), function($q) use($request){
            $orderBy = explode("|", $request['order_by']);
            $q->orderBy($orderBy[0], $orderBy[1]);
        })
        ->paginate($request['limit'] ?? 10);

        return $this->success($blogs);
    }

    public function store(CreateBlogRequest $request)
    {
        $file = $request->file('img');
        $filePath = $file->store('blogs', ['disk' => 'upload']);
        $blog = Blog::create([
            'img' => $filePath,
            'name' => $request['name'],
            'status' => $request['status'] ?? 0,
            'desc' => $request['desc'] ?? null,
            'content' => $request['content'] ?? null,
            'user_id' => auth()->user()->id ?? null
        ]);

        return $this->success($blog);
    }

    public function show($id)
    {
        $blog = Blog::select(
            '*',
            DB::raw('DATE_FORMAT(created_at, "%d-%m-%Y %H:%i") as creat_at')
        )
        ->where('id', $id)
        ->with('user')
        ->first();
        return $this->success($blog);
    }

    public function delete($id)
    {
        $blog = Blog::find($id) ?? null;
        Storage::disk('upload')->delete($blog->img);
        $result = $blog->delete();
        return $this->success($result);
    }

    public function updateStatus(Request $request, $id)
    {
        $blog = Blog::where('id', $id)->update([
            'status' => $request['status']
        ]);
        return $this->success($blog);
    }

    public function getOutBlogList(Request $request)
    {
        $blog = Blog::when(isset($request['other_id']), function($q) use($request) {
            $q->where('id', '<>', $request['other_id']);
        })
        ->paginate($request['limit']);
        return $this->success($blog);
    }

    public function update(UpdateBlogRequest $request, $blogId)
    {
        $blog = Blog::find($blogId) ?? null;
        $file = $request->file('img');
        if($request->hasFile('img')){
            Storage::disk('upload')->delete($blog->img);
            $blog->img = $file->store('blogs', ['disk' => 'upload']);
        }
        $blog->name = $request['name'];
        $blog->desc = $request['desc'];
        $blog->content = $request['content'];
        $blog->status = $request['status'];
        $result = $blog->save();
        return $this->success($result);
    }
}
