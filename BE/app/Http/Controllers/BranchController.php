<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBranchRequest;
use App\Http\Requests\UpdateBranchRequest;
use App\Models\Branch;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BranchController extends Controller
{
    public function get(Request $request)
    {
        $branchs = Branch::withCount('products')->paginate($request['limit'] ?? 999);
        return $this->success($branchs);
    }

    public function createBranch(CreateBranchRequest $request)
    {
        $file = $request->file('image');
        $filePath = $file->store('branchs', ['disk' => 'upload']);
        $branch = Branch::create([
            'img' => $filePath,
            'name' => $request['name']
        ]);

        return $this->success($branch);
    }

    public function deleteBranch($branchId)
    {
        $branch = Branch::find($branchId) ?? null;
        Storage::disk('upload')->delete($branch->img);
        $result = $branch->delete();
        return $this->success($result);
    }

    public function getDetail($branchId)
    {
        $branch = Branch::find($branchId);
        return $this->success($branch);
    }

    public function updateBranch(UpdateBranchRequest $request, $branchId)
    {
        $branch = Branch::find($branchId);
        if($request->hasFile('image')){
            Storage::disk('upload')->delete($branch->img);
            $branch->img = $request->file('image')->store('branchs', ['disk' => 'upload']);
        }
        $branch->name = $request['name'];
        $branch->save();
        return $this->success("Cập nhật thành công !");
    }
}
