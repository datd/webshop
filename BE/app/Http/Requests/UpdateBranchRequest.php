<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBranchRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'image' => 'nullable|mimes:jpg,jpeg,png,webp,jfif|max:2000'
        ];
    }
}
