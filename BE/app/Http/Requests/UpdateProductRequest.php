<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends BaseRequest
{

    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'price' => 'required|numeric',
            'branch_id' => 'nullable|exists:branchs,id',
            'desc' => 'nullable|max:255',
            'image' => 'nullable|mimes:jpg,jpeg,png,webp|max:2000'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng điền tên sản phẩm !',
            'name.max' => 'Tên sản phẩm tối đa :max ký tự !',

            'price.required' => 'Vui lòng điền giá !',
            'price.numeric' => 'Giá phải là số !',
            
            'branch_id.exists' => 'Không tồn tại thương hiệu này !',
            
            'desc.max' => 'Không quá :max ký tự !',
            
            'image.mimes' => 'Phải là định dạng :mimes',
            'image.max' => 'Ảnh không quá :max kb'
        ];
    }
}
