<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductImageRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'images.*' => 'required|mimes:jpg,png,jpeg,webp|max:10000',
            'product_id' => 'required|exists:products,id'
        ];
    }
}
