<?php

namespace App\Http\Requests;

class CreateSliderRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'image' => 'required|max:10000|mimes:jpg,jpeg,png'
        ];
    }
}
