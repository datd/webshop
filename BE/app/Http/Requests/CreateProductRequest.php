<?php

namespace App\Http\Requests;

class CreateProductRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'price' => 'required|numeric|min:1|max:999999',
            'branch_id' => 'required|exists:branchs,id',
            'desc' => 'required|max:255',
            'image' => 'required|max:10000|mimes:jpg,jpeg,png,webp'
        ];
    }
}
