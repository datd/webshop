<?php

namespace App\Http\Requests;

class CreateBlogRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'img' => 'required|mimes:jpg,jpeg,png,webp,jfif|max:2000',
            'status' => 'nullable|boolean',
            'desc' => 'nullable|max:255|string',
            'content' => 'nullable|string',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng điền tên bài viết !',
            'name.max' => 'Tên bài viết không quá :max ký tự !',

            'img.required' => 'Vui lòng chọn ảnh !',
            'img.mimes' => 'Ảnh sai định dạng !',
            'img.max' => 'Không quá :max KB !',

            'status.boolean' => 'Status là kiểu boolean !',

            'desc.max' => 'Mô tả không quá :max ký tự !',
            'desc.string' => 'Phải là dạng chuỗi !',

            'content.string' => 'Nội dung phải là dạng chuỗi !'
        ];
    }
}
