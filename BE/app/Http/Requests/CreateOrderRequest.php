<?php

namespace App\Http\Requests;

class CreateOrderRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'name' => 'required|max:50',
            'email' => 'required|email|max:255',
            'phone' => 'required|digits_between:10,12',
            'address' => 'required|max:255'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng tên của bạn !',
            'name.max' => 'Tên không vượt quá :max ký tự !',

            'email.required' => 'Vui lòng điền email !',
            'email.email' => 'Email sai format !',
            'email.max' => 'Email không quá :max ký tự !',

            'phone.required' => 'Vui lòng điền số điện thoại !',
            'phone.digits_between' => 'Số điện thoại từ :min - :max ký tự !',

            'address.required' => 'Vui lòng điền địa chỉ !',
            'address.max' => 'Địa chỉ không quá :max ký tự !'
        ];
    }
}
