<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBlogRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'desc' => 'required|max:255',
            'status' => 'required|boolean',
            'content' => 'nullable|string',
            'img' => 'nullable|mimes:jpg,jpeg,png,webp|max:2000'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng điền tiêu đề !',
            'name.max' => 'Tiêu đề không quá :max ký tự !',
            'desc.required' => 'Vui lòng nhập mô tả !',
            'desc.max' => 'Mô tả không quá :max ký tự !',
            'status.required' => 'Vui lòng điền status !',
            'content.string' => 'Nội dung phải là chuỗi !',
            'img.mimes' => 'Ảnh phải là :mimes !',
            'img.max' => 'Ảnh không quá :max ký tự !'
        ];
    }
}
