<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateBranchRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'image' => 'required|mimes:jpg,jpeg,jfif,webp,png|max:2000',
            'name' => 'required|max:255'
        ];
    }

    public function messages()
    {
        return [
            'image.required' => 'Vui lòng chọn ảnh !',
            'image.mimes' => 'Ảnh phải là định dạng :mimes!',
            'image.max' => 'Ảnh không quá :max kb !',

            'name.required' => 'Vui lòng điền tên thương hiệu !',
            'name.max' => 'Tên thương hiệu không quá :max ký tự !'
        ];
    }
}
