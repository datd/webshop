<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class ConfirmOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $mailTo;
    protected $mailContent;

    public function __construct($mailTo, $mailContent)
    {
        $this->mailTo = $mailTo;
        $this->mailContent = $mailContent;
    }

    public function handle()
    {
        $mailTo = $this->mailTo;
        $mailContent = $this->mailContent;
        Mail::send('mails/confirm_order', $mailContent, function($msg) use($mailTo){
            $msg->to($mailTo)->subject("Xác nhận đơn hàng !");
        });
    }
}
