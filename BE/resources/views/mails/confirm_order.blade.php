<h3>Thông tin đơn hàng</h3>
<div>
    <b>Họ tên: </b>
    <span>{{ $name }}</span>
</div>
<div>
    <b>Địa chỉ: </b>
    <span>{{ $address }}</span>
</div>
<div>
    <b>Email: </b>
    <span>{{ $email }}</span>
</div>
<div>
    <b>Số điện thoại: </b>
    <span>{{ $phone }}</span>
</div>
<h5>
    Sản phẩm đã đặt
</h5>
<table border="1">
    <thead>
        <tr>
            <th>Tên sản phẩm</th>
            <th>Size</th>
            <th>Số lượng</th>
            <th>Giá</th>
            <th>Thành tiền</th>
        </tr>
    </thead>
    <tbody>
        @foreach($products as $product)
        <tr>
            <td>{{$product['name']}}</td>
            <td>{{$product['pivot']['size']}}</td>
            <td>{{$product['pivot']['qty']}}</td>
            <td>{{number_format($product['pivot']['price'], 0, '', ',')}}đ</td>
            <td>{{number_format($product['pivot']['price'] * $product['pivot']['qty'], 0, '', ',')}}đ</td>
        </tr>
        @endforeach
    </tbody>
</table>
@php
    $total = 0;
    foreach($products as $product){
        $total += $product['pivot']['price'] * $product['pivot']['qty'];
    }
@endphp
<b style="padding-top: 15px">Tổng thanh toán: {{number_format($total, 0, '', ',')}}đ</b>
