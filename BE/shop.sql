-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th7 13, 2022 lúc 11:24 AM
-- Phiên bản máy phục vụ: 10.4.24-MariaDB
-- Phiên bản PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `shop`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `desc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view_count` int(11) DEFAULT 0,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `blogs`
--

INSERT INTO `blogs` (`id`, `name`, `img`, `status`, `desc`, `content`, `view_count`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Giới thiệu', 'blogs/JnJgOtvZKoDFwO5buNoCj3mDzgJ5ts2KCBgBrrJk.jpg', 1, 'Đây là bài giới thiệu về website bán hàng Laravel + ReactJS', '<h3>Phía Backend</h3><ul><li>Framework: Laravel</li><li>Thư viện hỗ trợ: Bumbummen99/ShoppingCart</li><li>Kĩ thuật sử dụng: Laravel Route Api, Job Queue, Migration, Seeder, Controller, Model, Validator, Mailer, View,…</li></ul><h3>Phía Frontend</h3><ul><li>Frameword, library: RreactJS, Bootstrap 4</li><li>Thư viện hỗ trợ: React Router DOM, Axios, AntDesign, Redux Thunk,…</li><li>Kỹ thuật sử dụng: React props, state, hooks function</li></ul>', 0, 1, '2022-07-13 02:11:12', '2022-07-13 02:11:12'),
(2, 'TẠI SAO CÁC CÔNG TY THƯỜNG DÙNG ĐỂ XÂY DỰNG DỰ ÁN?', 'blogs/DD7FZdXHQk3z5EZmlo4euXOfvJUPRvxC4enLsAOq.png', 1, 'TẠI SAO CÁC CÔNG TY THƯỜNG DÙNG ĐỂ XÂY DỰNG DỰ ÁN?', '<h3><strong>Laravel là gì?</strong></h3><p>- Được phát triển dựa trên mô hình MVC, Laravel là một<a href=\"https://wiki.tino.org/cach-su-dung-phpmyadmin/\">&nbsp;PHP</a>&nbsp;<a href=\"https://wiki.tino.org/framework-la-gi/\">Framework</a>&nbsp;mã nguồn mở miễn phí với cú pháp rõ ràng, mạch lạc.<br>- Framework&nbsp;hoặc “Software Framework” (tạm dịch: khung phần mềm) là “bộ khung” cung cấp đa số các kiểu mẫu thiết kế phù hợp với ứng dụng bạn sắp thực hiện, các thư viện, API, trình biên dịch. Framework được cấu thành từ các đoạn code.</p><p>MVC (Model-View-Controller) là mô hình phân bố source code thành 3 phần. Mỗi thành phần có một nhiệm vụ riêng biệt và độc lập với các thành phần khác. Cụ thể là:</p><ul><li><strong>Model :</strong>&nbsp;Đây là nơi chứa những nghiệp vụ tương tác với dữ liệu hoặc hệ quản trị cơ sở dữ liệu (mysql, mssql…). Thành phần Model bao gồm các class/function xử lý nhiều nghiệp vụ như kết nối database, truy vấn dữ liệu, thêm – xóa – sửa dữ liệu, …</li><li><strong>View :</strong>&nbsp;Đây là nơi chứa những giao diện như nút bấm, khung nhập, menu, hình ảnh, … Thành phần View sẽ đảm nhiệm nhiệm vụ hiển thị dữ liệu và giúp người dùng tương tác với hệ thống.</li><li><strong>Controller :</strong>&nbsp;Đây là nơi tiếp nhận những yêu cầu xử lý được gửi từ người dùng. Thành phần Controller sẽ gồm những class/ function xử lý nhiều nghiệp vụ logic giúp lấy đúng dữ liệu thông tin cần thiết và hiển thị dữ liệu đó ra cho người dùng qua lớp View.</li></ul><h3><strong>Lịch sử của Lavarel</strong></h3><p>Vốn là một .NET developer, khoảng năm 2010 – 2011, khi bắt đầu tiếp xúc với PHP, Taylor Otwell đã chọn CodeIgniter với những tính năng thịnh hành “vượt mặt” cả “anh lớn” Symfony. Trong suốt thời gian làm việc với CodeIgniter, anh sớm nhận ra những hạn chế nhất định.</p><p>Nhằm khắc phục những hạn chế này, đồng thời phát huy khả năng xuất sắc về design-pattern của mình, Taylor quyết định tạo ra một Framework mới. Tiêu chí anh đặt ra là đơn giản, dễ hiểu, hỗ trợ developer thực hiện ý tưởng nhanh nhất bằng nhiều tính năng hỗ trợ.</p><p>Tháng 6/2011, Taylor Otwell đã “trình làng” Lavarel như một giải pháp thay thế cho CodeIgniter. Với giải pháp này, lập trình viên (developer) đã được hỗ trợ nhiều tính năng mới mẻ với thao tác vô cùng đơn giản. Eloquent ORM mạnh mẽ, xác thực đơn giản, phân trang hiệu quả, …là những tính năng thu hút sự chú ý của đông đảo người dùng của phiên bản đầu tiên này.</p><p>Những ưu điểm vượt trội đó đã giúp Laravel nhanh chóng chiếm được chỗ đứng trên thị trường và phát triển mạnh mẽ.</p><p>Laravel hiện được phát hành theo giấy phép MIT, với source code được lưu trữ tại Gitthub. Tính đến thời điểm hiện tại, Laravel đã phát triển đến phiên bản 5.8 với nhiều cải tiến.</p>', 0, 1, '2022-07-13 02:16:06', '2022-07-13 02:16:06'),
(3, 'Giới thiệu ReactJS', 'blogs/5d5tVyoA1I7TNhOgREbPO5u2pb7O98tIGE6vdTQH.png', 1, 'ReactJS là thư viện JavaScript được sử dụng để xây dựng các thành phần UI có thể tái sử dụng.', '<p>ReactJS là thư viện JavaScript được sử dụng để xây dựng các thành phần UI có thể tái sử dụng. Theo tài liệu chính thức của React, thì ReacJS được định nghĩa như sau :&nbsp;</p><p>React là một thư viện để xây dựng giao diện người dùng. Nó khuyến khích việc tạo ra các thành phần UI có thể tái sử dụng, trình bày dữ liệu thay đổi theo thời gian. Rất nhiều người sử dụng React như V trong MVC. React trừu tượng hóa DOM , cung cấp một mô hình lập trình đơn giản hơn và hiệu suất tốt hơn. React cũng có thể kết xuất trên máy chủ bằng NodeJS và nó có thể cung cấp nguồn tài nguyên cho các ứng dụng gốc bằng React Native. React thực hiện luồng dữ liệu dữ liệu một chiều, làm giảm khối lượng truyền tải và dễ dàng hơn so với ràng buộc dữ liệu truyền thống.</p><p>&nbsp;</p><h2>Tính năng của React :</h2><ul><li><strong>JSX -&nbsp;</strong><i>JSX&nbsp;</i>là phần mở rộng cú pháp JavaScript. Không cần thiết phải sử dụng JSX trong phát triển React, nhưng khuyến khích nên sử dụng</li><li><strong>Component :</strong>Tất cả trong&nbsp;<i>Reat</i>&nbsp;được cấu thành từ&nbsp;<i>component</i>. Bạn cần nghĩ về mọi thứ như một&nbsp;<i>component</i>. Điều này sẽ giúp bạn duy trì khi làm việc trên các dự án quy mô scale nhanh chóng</li><li><strong>Luồng dữ liệu đơn hướng và thông lượng</strong>&nbsp;-&nbsp;<i>React&nbsp;</i>thực hiện luồng dữ liệu một chiều giúp dễ dàng suy luận về ứng dụng của bạn. Thông lượng là một mô hình giúp giữ cho dữ liệu của bạn một chiều..</li><li><strong>Giấy phép</strong>&nbsp;-&nbsp;<i>React&nbsp;</i>được cấp phép theo Tài liệu&nbsp;<i>Facebook&nbsp;</i>Inc. được cấp phép theo CC BY 4.0</li></ul><p><br>&nbsp;</p>', 0, 1, '2022-07-13 02:17:39', '2022-07-13 02:17:39');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `branchs`
--

CREATE TABLE `branchs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `branchs`
--

INSERT INTO `branchs` (`id`, `name`, `img`, `created_at`, `updated_at`) VALUES
(1, 'Adidas', 'branchs/SkVP05hYQO9Whgx9axHywZbocyZP1DoguNkuaR9N.png', '2022-07-13 01:04:35', '2022-07-13 01:19:29'),
(2, 'Nike', 'branchs/eQ0nsHIohsm93eZ3HAf7k6k9Ui2FBoebarMce0AB.png', '2022-07-13 01:04:35', '2022-07-13 01:20:04'),
(3, 'Fila', 'branchs/LAbHjTe6bHOkEIvyqleBgU6KCsvyFxjviYQN92DZ.png', '2022-07-13 01:04:35', '2022-07-13 01:21:01'),
(4, 'Vans', 'branchs/TjCUKX8pnypn8JEG2sDjofX8ce6NdLKbAnoTWe6Q.png', '2022-07-13 01:04:35', '2022-07-13 01:21:47'),
(5, 'Converse', 'branchs/LSzvPDzXUdSYri04vKnVVsKHOGOTfBWkJuytONF4.png', '2022-07-13 01:04:35', '2022-07-13 01:22:32'),
(6, 'Kappa', 'branchs/xPtcpEKReus9Bx8l7HOuP1KYqBBDzP099K5JWFZB.png', '2022-07-13 01:04:35', '2022-07-13 01:23:47');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `carts`
--

CREATE TABLE `carts` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchaser_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchaser_id` bigint(20) UNSIGNED DEFAULT NULL,
  `discount_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_amount` int(11) NOT NULL DEFAULT 0,
  `custom_fields` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`custom_fields`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cart_items`
--

CREATE TABLE `cart_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cart_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchaseable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchaseable_id` bigint(20) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL DEFAULT 1,
  `unit_price` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `custom_fields` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`custom_fields`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2020_12_13_000001_create_carts_table', 1),
(6, '2020_12_13_000002_create_cart_items_table', 1),
(7, '2022_07_05_094448_create_table_slider', 1),
(8, '2022_07_06_024423_create_table_brands', 1),
(9, '2022_07_06_024914_create_table_product', 1),
(10, '2022_07_06_025557_add_column_foreign_key_product', 1),
(11, '2022_07_06_055448_update_price_data_type', 1),
(12, '2022_07_06_082729_creat_table_product_image', 1),
(13, '2022_07_09_160754_create_table_order', 1),
(14, '2022_07_09_162354_create_table_detail_order', 1),
(15, '2022_07_09_170528_create_jobs_table', 1),
(16, '2022_07_09_175908_add_column_to_product_order', 1),
(17, '2022_07_13_000356_create_table_blog', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `name`, `email`, `phone`, `address`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn Văn A', 'qwekqlwemqjlkwen@mailinator.com', '0123123123', 'TPHCM', 1, '2022-07-13 02:18:34', '2022-07-13 02:18:34');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 1, 'login-token', '099ecc5631464221c9e0ba32a67f1a83d94f46e43b93c82ce56f087d00a3ead1', '[\"*\"]', '2022-07-13 02:19:26', '2022-07-13 01:05:28', '2022-07-13 02:19:26');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `desc`, `img`, `branch_id`, `created_at`, `updated_at`) VALUES
(1, 'FAPAS TEDDY BROWN TEE', 290000, '- ĐỔI sản phẩm trong vòng 07 ngày trên toàn HỆ THỐNG CỬA HÀNG\r\n- Không áp dụng ĐỔI sản phẩm trong CTKM', 'products/IpXrc9FwYS0fcO0BrytxnT43TM04bLHoJaMK2Wb9.webp', 2, '2022-07-13 01:26:14', '2022-07-13 01:26:14'),
(2, 'FAPAS RAW BASIC TEE', 230000, '- Thành viên SILVER được giảm 5% khi tích đủ 5 triệu\r\n- Thành viên GOLD được giảm 10% khi tích đủ 12 triệu\r\n- Thành viên DIAMOND được giảm 15% khi tích đủ 28 triệu', 'products/6kYu89MiFbftTVmFWogSPjkXIRIMssfhNcqdoHCv.webp', 4, '2022-07-13 01:28:37', '2022-07-13 01:28:37'),
(3, 'FAPAS REGULAR TOVIA TEE', 300000, 'Tư vấn mua hàng: 0123123123\r\n- ĐỔI sản phẩm trong vòng 07 ngày trên toàn HỆ THỐNG CỬA HÀNG\r\n- Không áp dụng ĐỔI sản phẩm trong CTKM', 'products/ZWq9RZSNpc8UteKYB1aTdVlF8KZpdQ4kRLJ1noqO.jpg', 1, '2022-07-13 01:30:48', '2022-07-13 01:30:48'),
(4, 'FAPAS WASHED BAT TEE', 290000, 'Mã sản phẩm ATF220403401\r\n- ĐỔI sản phẩm trong vòng 07 ngày trên toàn HỆ THỐNG CỬA HÀNG', 'products/V3Mhd0UQyXvBQFhPrKvnwdAYz2wIjqL8CYJ8Dawq.jpg', 4, '2022-07-13 01:32:59', '2022-07-13 01:32:59'),
(5, 'FAPAS B&W TEE', 290000, 'Mã sản phẩm ATF220403301\r\n- ĐỔI sản phẩm trong vòng 07 ngày trên toàn HỆ THỐNG CỬA HÀNG\r\n- Không áp dụng ĐỔI sản phẩm trong CTKM', 'products/4U9C1NOL8alKfDOE6vl4yHi5V48arapysX6dhn5E.webp', 5, '2022-07-13 01:35:00', '2022-07-13 01:35:00'),
(6, 'FAPAS T&J WASH TEE', 900000, 'Mã sản phẩmATF220402901\r\n- ĐỔI sản phẩm trong vòng 07 ngày trên toàn HỆ THỐNG CỬA HÀNG\r\n- Không áp dụng ĐỔI sản phẩm trong CTKM', 'products/VjGoAnWNPwRlyRR9HT3WWuDAtw2Wt3rumYWRq713.jpg', 6, '2022-07-13 01:36:36', '2022-07-13 01:52:51'),
(7, 'MICKEY STRIPED TEE', 320000, 'Mã sản phẩm :ATF220403001\r\n- ĐỔI sản phẩm trong vòng 07 ngày trên toàn HỆ THỐNG CỬA HÀNG\r\n- Không áp dụng ĐỔI sản phẩm trong CTKM', 'products/o7NWiD8X7661xmgXkURqSY1dEsm2m3YnO4IG20p0.jpg', 6, '2022-07-13 01:40:51', '2022-07-13 01:40:51'),
(8, 'FAPAS CAMO 9 TEE', 500000, '- ĐỔI sản phẩm trong vòng 07 ngày trên toàn HỆ THỐNG CỬA HÀNG\r\n- Không áp dụng ĐỔI sản phẩm trong CTKM', 'products/7fwNsxECQKptKZ1EtWYFB7XYR82W0B74eXNzOD1y.jpg', 5, '2022-07-13 01:52:11', '2022-07-13 01:52:11'),
(9, 'WASH EAGLE TEE', 650000, '- ĐỔI sản phẩm trong vòng 07 ngày trên toàn HỆ THỐNG CỬA HÀNG\r\n- Không áp dụng ĐỔI sản phẩm trong CTKM\r\n- Thành viên SILVER được giảm 5% khi tích đủ 5 triệu\r\n- Thành viên GOLD được giảm 10% khi tích đủ 12 triệu', 'products/TBdJ6iYkikibEOSJAcZZmbqTONYIyReck5lC4HMo.webp', 6, '2022-07-13 01:55:22', '2022-07-13 01:55:22'),
(10, 'MYP PLAIN TEE', 100000, '- Thành viên SILVER được giảm 5% khi tích đủ 5 triệu\r\n- Thành viên GOLD được giảm 10% khi tích đủ 12 triệu\r\n- Thành viên DIAMOND được giảm 15% khi tích đủ 28 triệu', 'products/vO2SCmI40r6XV9YkOc4tUWerUXw6DSA0cfQSJfNa.webp', 3, '2022-07-13 01:56:46', '2022-07-13 01:56:46'),
(11, 'RACING TEAM TEE', 250000, '- ĐỔI sản phẩm trong vòng 07 ngày trên toàn HỆ THỐNG CỬA HÀNG\r\n- Không áp dụng ĐỔI sản phẩm trong CTKM', 'products/f5zXbJrXOlM0z9mU1zpqjSF8OiqCb6xnx1GPu7gA.webp', 5, '2022-07-13 01:58:07', '2022-07-13 01:58:07'),
(12, 'ZEUS GREEK TEE', 300000, '- ĐỔI sản phẩm trong vòng 07 ngày trên toàn HỆ THỐNG CỬA HÀNG\r\n- Không áp dụng ĐỔI sản phẩm trong CTKM', 'products/paNi4Z812dxaNlT0XSQCEkpdruR4wS1GVRAGv8qq.jpg', 4, '2022-07-13 02:00:24', '2022-07-13 02:00:24'),
(13, 'HALF DENIM TEE', 400000, '- ĐỔI sản phẩm trong vòng 07 ngày trên toàn HỆ THỐNG CỬA HÀNG\r\n- Không áp dụng ĐỔI sản phẩm trong CTKM', 'products/WyV9kiaGIS7mBDVff7HNLBUdbn44jSxkkFN9ln9R.webp', 3, '2022-07-13 02:02:01', '2022-07-13 02:02:01'),
(14, 'CHECKED WHITE NECK TEE', 300000, '- ĐỔI sản phẩm trong vòng 07 ngày trên toàn HỆ THỐNG CỬA HÀNG\r\n- Không áp dụng ĐỔI sản phẩm trong CTKM', 'products/sXu5ToPklJCbSbvZAgdCRWyqYsEzYXehPMWkbIBD.jpg', 5, '2022-07-13 02:03:18', '2022-07-13 02:03:18');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products_orders`
--

CREATE TABLE `products_orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED DEFAULT NULL,
  `order_id` bigint(20) UNSIGNED DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products_orders`
--

INSERT INTO `products_orders` (`id`, `product_id`, `order_id`, `price`, `qty`, `created_at`, `updated_at`) VALUES
(1, 7, 1, 320000, 1, NULL, NULL),
(2, 1, 1, 290000, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_images`
--

INSERT INTO `product_images` (`id`, `img`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 'product_images/l7B6DPLLfgWqMo14eg6ForbyjJGDwSUCbyvPlCUc.webp', 1, '2022-07-13 01:26:26', '2022-07-13 01:26:26'),
(2, 'product_images/acwUKHgoggNQYmnR3k4jOoLWD1PWdxp8cIAqofow.webp', 1, '2022-07-13 01:26:26', '2022-07-13 01:26:26'),
(3, 'product_images/ADOHehdTsQ1ibzywWIYLhpGdSGp1qd9z7ttVfP1u.webp', 1, '2022-07-13 01:26:26', '2022-07-13 01:26:26'),
(4, 'product_images/JmkpDiiMjhpZNOcO4r2CeqbDw8DXkEHCEHNvnozz.webp', 1, '2022-07-13 01:26:26', '2022-07-13 01:26:26'),
(5, 'product_images/Q819JJpCgVD8JHZghR9uIieXfVZEoIeCjS1SLfft.webp', 2, '2022-07-13 01:29:25', '2022-07-13 01:29:25'),
(6, 'product_images/vp3APUXUWoZ6oACsyHMswaTyugPWXxwNzFJAdg3E.webp', 2, '2022-07-13 01:29:25', '2022-07-13 01:29:25'),
(7, 'product_images/KQO3kdYUbfEw0bJoUTRyoS5HMvqxy0nOSkzuUfag.webp', 2, '2022-07-13 01:29:25', '2022-07-13 01:29:25'),
(8, 'product_images/3SzsIJ5esF4KrGjT3gdNuoYeuB4SGADlwunzRUOr.jpg', 3, '2022-07-13 01:30:57', '2022-07-13 01:30:57'),
(9, 'product_images/ajcxTZsxuEglazNKj6kYHU5r67sG13n4qHfaTQeO.jpg', 3, '2022-07-13 01:30:57', '2022-07-13 01:30:57'),
(10, 'product_images/J4YNxsh8rezcAV3zUUyvjuwj0KA7W6x5DyUlTKvz.jpg', 3, '2022-07-13 01:30:57', '2022-07-13 01:30:57'),
(11, 'product_images/QBauUgwEZMhzz3hnTR7D6yGLr39EV9mFybDilQQD.webp', 4, '2022-07-13 01:33:09', '2022-07-13 01:33:09'),
(12, 'product_images/oTZ0qiET84ud9PE2TwBw4aOCnqCuXStkZV54SwhK.jpg', 4, '2022-07-13 01:33:09', '2022-07-13 01:33:09'),
(13, 'product_images/N4VVX8GKmedulzRRUf8YEFj00M65K0dJBqUllMjn.jpg', 4, '2022-07-13 01:33:09', '2022-07-13 01:33:09'),
(14, 'product_images/xkYhlsyjHAHX7TBLoViP4dSR2tdOH2nJuDOpWUWl.jpg', 4, '2022-07-13 01:33:09', '2022-07-13 01:33:09'),
(15, 'product_images/nKHaDdAz8dzVe3X6rSGFiAr5Q8cb7oRCI8Sf7lvu.webp', 5, '2022-07-13 01:35:11', '2022-07-13 01:35:11'),
(16, 'product_images/W7E77qtwRiSB2qr3oE0RBp3SQwsYwG8yDedRJdCv.webp', 5, '2022-07-13 01:35:11', '2022-07-13 01:35:11'),
(17, 'product_images/L1Q2bKqe8sv70ZOJunxez9dvRQssYNQRfenJByja.webp', 5, '2022-07-13 01:35:11', '2022-07-13 01:35:11'),
(18, 'product_images/pAtwJ9Q0v3BTEzzvFTztkVJfoB3ndU9ae01mS9cN.webp', 5, '2022-07-13 01:35:11', '2022-07-13 01:35:11'),
(19, 'product_images/QeofgdyBsJSGP2WpFZKD20KAxZqG7HedyIIASrLT.jpg', 6, '2022-07-13 01:36:50', '2022-07-13 01:36:50'),
(20, 'product_images/CLHqW6OGqteX3roDsT47BOpYqJu9sFtavZCHVMTc.jpg', 6, '2022-07-13 01:36:50', '2022-07-13 01:36:50'),
(21, 'product_images/BLxCvQOoDf7EmwTDvVYoETuz9wj4zUO6kT6jeyuC.jpg', 6, '2022-07-13 01:36:51', '2022-07-13 01:36:51'),
(22, 'product_images/sgeWLlJenQ4IvzhrLU48kUL1GleJfNtJIVD6LPgk.jpg', 7, '2022-07-13 01:41:00', '2022-07-13 01:41:00'),
(23, 'product_images/HhsshAEe2X63DvsqjPsP6XltBsLIbprBkl07yJVa.jpg', 7, '2022-07-13 01:41:00', '2022-07-13 01:41:00'),
(24, 'product_images/idsYPBlq3fZ8mLQbD9TApY7jeX4FQBXoD66gkdzY.webp', 7, '2022-07-13 01:41:00', '2022-07-13 01:41:00'),
(25, 'product_images/Zi7qOELg6AEnoUJRp8ZoBmsAzIJ75OQg4eekWQOu.jpg', 7, '2022-07-13 01:41:00', '2022-07-13 01:41:00'),
(26, 'product_images/CHiwYG8kXc1KCv8aWaERbhEg8sCvfjoYtv4Q60E3.jpg', 8, '2022-07-13 01:52:28', '2022-07-13 01:52:28'),
(27, 'product_images/IVc7L4ywcOu4fbX6bpN0BRdwfMeZSPn8FmGIob7L.webp', 8, '2022-07-13 01:52:28', '2022-07-13 01:52:28'),
(28, 'product_images/3TXECUhJlTWUlTyzO6fD7wSv7xUn8IHtrFG4wdCK.jpg', 8, '2022-07-13 01:52:28', '2022-07-13 01:52:28'),
(29, 'product_images/V0e8Cd5mHclz6n1kefyrGxXUkOO0g8xtWvKwsleB.jpg', 8, '2022-07-13 01:52:28', '2022-07-13 01:52:28'),
(30, 'product_images/RaTNysc81c36WZCrrTtLTb9ouYLF5M6bNcYvtrQD.webp', 9, '2022-07-13 01:55:30', '2022-07-13 01:55:30'),
(31, 'product_images/dlbbdDpPBeOhBBROzIfR20GaS4HUsz9YjvSworvJ.webp', 9, '2022-07-13 01:55:30', '2022-07-13 01:55:30'),
(32, 'product_images/bs6hMvLyvCUU94vtWMg1aapK224iKeSAHHJJzm8e.jpg', 9, '2022-07-13 01:55:30', '2022-07-13 01:55:30'),
(33, 'product_images/aWdjZRj8ixWXIAQ9ufmQnyWrbsHHqrks0jr8DibT.webp', 9, '2022-07-13 01:55:30', '2022-07-13 01:55:30'),
(34, 'product_images/fAQ23pdPe99HFX8hn0WIZD5OqUpH6TiXOFmRgIvE.webp', 10, '2022-07-13 01:56:54', '2022-07-13 01:56:54'),
(35, 'product_images/jzndkfUT8BXR5DRS7A8X2HnakjlN8GuKU5uSah5h.webp', 10, '2022-07-13 01:56:54', '2022-07-13 01:56:54'),
(36, 'product_images/R6Zc3P3ZzDyBKk9lXWXQCXq17vcn5lAeHM0rNamt.webp', 10, '2022-07-13 01:56:54', '2022-07-13 01:56:54'),
(37, 'product_images/hrjSlyU43eScBOhJveAnCddzWXGymLpnYtDBi7Nv.webp', 10, '2022-07-13 01:56:54', '2022-07-13 01:56:54'),
(38, 'product_images/LK8sOCKPPuBic7YSB0NvgAbDP2s7LJmW2sP2rqDi.webp', 11, '2022-07-13 01:58:16', '2022-07-13 01:58:16'),
(39, 'product_images/EMQPX3Ewif4J18Nhu7oLMeVSmoNMQgEMG405x2sU.webp', 11, '2022-07-13 01:58:16', '2022-07-13 01:58:16'),
(40, 'product_images/F0TF6vPIDAGO8PgeFGHaZyDO9PJTDY1cPoFhvmjl.webp', 11, '2022-07-13 01:58:16', '2022-07-13 01:58:16'),
(41, 'product_images/8KFTn96LozAFzXLS6i8XujRW0lgGSxCCBqmLArpy.webp', 11, '2022-07-13 01:58:16', '2022-07-13 01:58:16'),
(42, 'product_images/1XDPt9GTiUOyqoiGwY86qwwZH3uRVl00PTZe0Olc.jpg', 12, '2022-07-13 02:00:31', '2022-07-13 02:00:31'),
(43, 'product_images/1pIo8LodsUKSEOZpVmM52ge2vd6ZrAHTAowcPobp.jpg', 12, '2022-07-13 02:00:31', '2022-07-13 02:00:31'),
(44, 'product_images/MC6JXOKQoDPXQloeXXXoxvkFPh35IlG6tgAESeJ8.jpg', 12, '2022-07-13 02:00:31', '2022-07-13 02:00:31'),
(45, 'product_images/R2PhBdzgGgZ2JTeXpNKnyHh93q6qwHeMYJo3P2wZ.webp', 12, '2022-07-13 02:00:31', '2022-07-13 02:00:31'),
(46, 'product_images/YJuihhdlgykvzQz4gFXQHcMoGXcQd5om35pMwJjt.webp', 13, '2022-07-13 02:02:09', '2022-07-13 02:02:09'),
(47, 'product_images/rTIYDEMh4DpuO8eozptiBxwLMRhIE1fqbg1PYxTF.webp', 13, '2022-07-13 02:02:09', '2022-07-13 02:02:09'),
(48, 'product_images/eMJkieuJ0CWiS3pDINGzTWYygN1RVuqrcgwB94Il.webp', 13, '2022-07-13 02:02:09', '2022-07-13 02:02:09'),
(49, 'product_images/SNPm6cRNqjDdaPY9ZqDdPGbuMh4umeorG32f5Kmy.webp', 13, '2022-07-13 02:02:09', '2022-07-13 02:02:09'),
(50, 'product_images/vQ5G3MJMh3ejXf0HW6YCRtGyjJ7g8equFCldmu3b.jpg', 14, '2022-07-13 02:03:27', '2022-07-13 02:03:27'),
(51, 'product_images/AMFipL8LgpG3LnYmKJxiqaS5KauWgLmBGOOyQhTy.jpg', 14, '2022-07-13 02:03:27', '2022-07-13 02:03:27'),
(52, 'product_images/J2MuLMc94LUTFYxIiuJzLmopPKj3kNS290PlxcJ8.jpg', 14, '2022-07-13 02:03:27', '2022-07-13 02:03:27'),
(53, 'product_images/HRAijgxPdwyeBW1kfnVUXkchMlXWh9LXUos2Lx1H.jpg', 14, '2022-07-13 02:03:27', '2022-07-13 02:03:27');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sliders`
--

INSERT INTO `sliders` (`id`, `img`, `created_at`, `updated_at`) VALUES
(1, 'sliders/pHzsOCkuKAYnZeMxb0eQlzO92Y4V1K3AwxdORSgS.jpg', '2022-07-13 01:12:55', '2022-07-13 01:12:55'),
(2, 'sliders/1FO0yboj9kaIifCXvjD00xGPvlA7yKL84m7sMMwd.jpg', '2022-07-13 01:17:27', '2022-07-13 01:17:27');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Danh Đạt', 'danhdat71@gmail.com', NULL, '$2y$10$n4kdSqNKIFUAHxyJdUrLl.QuaJ1N7MUF.Hggepmb7bDGXjgVHBE/G', NULL, '2022-07-13 01:04:35', '2022-07-13 01:04:35');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blogs_user_id_foreign` (`user_id`);

--
-- Chỉ mục cho bảng `branchs`
--
ALTER TABLE `branchs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_purchaser_type_purchaser_id_index` (`purchaser_type`,`purchaser_id`);

--
-- Chỉ mục cho bảng `cart_items`
--
ALTER TABLE `cart_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_items_purchaseable_type_purchaseable_id_index` (`purchaseable_type`,`purchaseable_id`),
  ADD KEY `cart_items_cart_id_index` (`cart_id`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Chỉ mục cho bảng `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_branch_id_foreign` (`branch_id`);

--
-- Chỉ mục cho bảng `products_orders`
--
ALTER TABLE `products_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_orders_product_id_foreign` (`product_id`),
  ADD KEY `products_orders_order_id_foreign` (`order_id`);

--
-- Chỉ mục cho bảng `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`);

--
-- Chỉ mục cho bảng `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `branchs`
--
ALTER TABLE `branchs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `cart_items`
--
ALTER TABLE `cart_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `products_orders`
--
ALTER TABLE `products_orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT cho bảng `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Các ràng buộc cho bảng `cart_items`
--
ALTER TABLE `cart_items`
  ADD CONSTRAINT `cart_items_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`);

--
-- Các ràng buộc cho bảng `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branchs` (`id`) ON DELETE SET NULL;

--
-- Các ràng buộc cho bảng `products_orders`
--
ALTER TABLE `products_orders`
  ADD CONSTRAINT `products_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `products_orders_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE SET NULL;

--
-- Các ràng buộc cho bảng `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
