<?php

use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\BranchController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductImageController;
use App\Http\Controllers\SliderController;
use App\Models\Branch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/admin/login', [AuthenticationController::class, 'login'])->middleware('throttle:admin_login');
Route::get('/slider', [SliderController::class, 'get']);
Route::get('/branch', [BranchController::class, 'get']);
Route::get('/product', [ProductController::class, 'getList']);
Route::get('/product/{product_id}', [ProductController::class, 'getDetailProduct']);
Route::get('/blogs', [BlogController::class, 'getOutBlogList']);
Route::get('/blog/{blog_id}', [BlogController::class, 'show']);
Route::group(['middleware' => ['auth:sanctum']], function(){
    Route::get('/get-me', [AuthenticationController::class, 'getMe']);
    Route::post('/slider', [SliderController::class, 'store']);
    Route::delete('/slider/{slider_id}/delete', [SliderController::class, 'delete']);

    Route::post('/product', [ProductController::class, 'store']);
    Route::delete('/product/{product_id}', [ProductController::class, 'delete']);
    Route::post('/product/{product_id}', [ProductController::class, 'update']);

    Route::post('/product-image', [ProductImageController::class, 'store']);
    Route::get('/{product_id}/product-image', [ProductImageController::class, 'getList']);
    Route::delete('/{product_image_id}/product-image', [ProductImageController::class, 'delete']);

    Route::get('/orders', [OrderController::class, 'getListOrder']);
    Route::put('/order/{order_id}/update-status', [OrderController::class, 'updateOrderStatus']);
    Route::get('/order/{order_id}', [OrderController::class, 'getDetailOrder']);

    Route::post('/branch', [BranchController::class, 'createBranch']);
    Route::delete('/branch/{branch_id}', [BranchController::class, 'deleteBranch']);
    Route::get('/branch/{branch_id}', [BranchController::class, 'getDetail']);
    Route::post('/branch/{branch_id}', [BranchController::class, 'updateBranch']);

    Route::post('/blog', [BlogController::class, 'store']);
    Route::delete('/blog/{blog_id}', [BlogController::class, 'delete']);
    Route::get('/blog', [BlogController::class, 'getList']);
    Route::put('/blog/{blog_id}/update-status', [BlogController::class, 'updateStatus']);
    Route::post('/blog/{blog_id}/update', [BlogController::class, 'update']);

    Route::get('/logout', [AuthenticationController::class, 'logout']);
});