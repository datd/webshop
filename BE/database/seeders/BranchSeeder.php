<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date('Y-m-d H:i:s');
        DB::table('branchs')->delete();
        DB::table('branchs')->insert([
            [
                'name' => 'Branch 1',
                'img' => 'branchs/1.jpg',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Branch 2',
                'img' => 'branchs/2.png',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Branch 3',
                'img' => 'branchs/3.png',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Branch 4',
                'img' => 'branchs/4.webp',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Branch 5',
                'img' => 'branchs/5.png',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'name' => 'Branch 6',
                'img' => 'branchs/6.png',
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);
    }
}
